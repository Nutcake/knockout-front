FROM node:16.15

WORKDIR /usr/src/server
COPY . .
RUN yarn && yarn run build:prod
CMD node expressServer.js
