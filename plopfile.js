const state = require('./templates/state');

module.exports = function configurePlop(plop) {
  state(plop);
};