export const DELETED_USER_USERNAME = 'DELETED_USER';
export const isDeletedUser = (username) => username === DELETED_USER_USERNAME;
export const DELETED_POST_CONTENT = "This post's content has been removed by a moderator.";
export const isDeletedPost = (post) =>
  post.content === DELETED_POST_CONTENT && isDeletedUser(post.user.username);
