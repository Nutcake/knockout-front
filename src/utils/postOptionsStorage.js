export const customColorsAutoValue = 'custom';

export const customColorsKey = 'customColors';
export const themeKey = 'theme';
export const widthKey = 'width';
export const scaleKey = 'scale';

export const defaultTheme = 'dark';
export const defaultWidth = 'wide';
export const defaultScale = 'medium';

export const loadValue = (key, defaultValue, firstCheckKey = null, firstAutoValue = null) => {
  if (firstCheckKey !== null && localStorage.getItem(firstCheckKey)) {
    return firstAutoValue;
  }
  const value = localStorage.getItem(key);
  if (value === 'undefined' || value === 'null' || value === null) {
    return defaultValue;
  }
  try {
    return JSON.parse(value);
  } catch (error) {
    return defaultValue;
  }
};

export const saveValue = (key, value) => {
  const stringified = JSON.stringify(value);
  localStorage.setItem(key, stringified);
};

export const loadOptions = () => {
  return {
    color: loadValue(themeKey, defaultTheme, customColorsKey, customColorsAutoValue),
    width: loadValue(widthKey, defaultWidth),
    scale: loadValue(scaleKey, defaultScale),
  };
};

// todo: everything below this should be deprecated when the perma-switch to labs is made

export const loadDisplayCountrySettingFromStorage = () => {
  const displayCountryInfo = JSON.parse(localStorage.getItem(`displayCountryInfo`));

  if (displayCountryInfo) {
    return displayCountryInfo;
  }
  return false;
};

export const saveDisplayCountryInfoSettingToStorage = (data) => {
  const displayCountryInfo = JSON.stringify(data);

  localStorage.setItem(`displayCountryInfo`, displayCountryInfo);
};

export const loadExtraScriptingSettingFromStorage = () => {
  const extraScripting = JSON.parse(localStorage.getItem(`extraScripting`));

  if (extraScripting !== null) {
    return extraScripting;
  }
  return true;
};

export const saveExtraScriptingInfoSettingToStorage = (data) => {
  const extraScripting = JSON.stringify(data);

  localStorage.setItem(`extraScripting`, extraScripting);
};

export const loadDisplayNsfwFilterSettingFromStorage = () => {
  const nsfwFilter = JSON.parse(localStorage.getItem(`nsfwFilter`));

  if (nsfwFilter !== null) {
    return nsfwFilter;
  }
  return true;
};

export const saveDisplayNsfwFilterSettingToStorage = (data) => {
  const nsfwFilter = JSON.stringify(data);

  localStorage.setItem(`nsfwFilter`, nsfwFilter);
};

export const loadDisplayNsfwFilterSettingFromStorageBoolean = () => {
  const nsfwFilter = loadDisplayNsfwFilterSettingFromStorage();

  return nsfwFilter;
};
