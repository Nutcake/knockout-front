import dayjs from 'dayjs';

export const PUNCHY_FLAVORS = { UKRAINE: 'Ukraine', TRANS_RIGHTS: 'Trans Rights' };
const PUNCHY_FLAVOR_CHANCE = 0.5;

const UKRAINE_QUOTES = [
  // Main Ukraine override quotes, based on the main hashtags that have been used around
  'Free Ukraine',
  'Stand with Ukraine',

  // These ones are just extra ones for more flavour, can disable whichever ones we don't want
  'Stop Putin',
  '"Russian warship, go fuck yourself"', // Wanted a Ukrainian version of this too, but couldn't find a reliable source for the translation
  'No to war',
  'Glory to Ukraine! Glory to the heroes!',
  'Слава Україні! Героям слава!,', // Ukrainian ver. of above
];

const TRANS_RIGHTS_QUOTES = [
  'Trans rights are human rights',
  'LGBTQ+ friendly',
  'Punchy is an ally',
  'BLÅHAJ is your friend',
  'Support trans rights',
];

const HOLIDAY_QUOTES = [
  'Happy holidays!',
  "Summer's here!",
  "It's getting hot in here 🥵",
  'Ever wonder how Santa manages to wear those heavy clothes in December?',
  '☀️',
  '2023 edition',
  '🏄',
  '🎄',
  'This year, my gift to you is anime memes 🎁',
  "We didn't have enough money to put snow on the site this year, so we're going with a summer theme",
  'Cultural exchange edition',
];

export const randomPunchyFlavor = () => {
  if (Math.random() < PUNCHY_FLAVOR_CHANCE) return 'Normal';

  return PUNCHY_FLAVORS[
    Object.keys(PUNCHY_FLAVORS)[Math.floor(Math.random() * Object.keys(PUNCHY_FLAVORS).length)]
  ];
};

export const aprilFools = () => {
  const currentDate = dayjs();
  return currentDate.month() === 3 && currentDate.date() === 1 && currentDate.year() === 2023;
};

const isHolidays = () => {
  const currentDate = dayjs();

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  return Math.abs(currentDate.diff(christmasDate, 'day')) <= 7;
};

export const getEventHeaderLogo = (flavor) => {
  if (flavor === PUNCHY_FLAVORS.UKRAINE) {
    return {
      default: '/static/logo_ukraine_light.svg', // Default theme for logged out users is light mode, so use light mode variation by default
      light: '/static/logo_ukraine_light.svg',
      dark: '/static/logo_ukraine_dark.svg',
      classic: '/static/logo_ukraine_dark.svg',
    };
  }

  if (flavor === PUNCHY_FLAVORS.TRANS_RIGHTS) {
    return {
      default: '/static/logo_trans_rights.png',
      light: '/static/logo_trans_rights.png',
      dark: '/static/logo_trans_rights.png',
      classic: '/static/logo_trans_rights.png',
    };
  }

  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return { default: '/static/logo_october.png' };
  }

  if (isHolidays()) {
    return { default: '/static/logo_summer.svg' };
  }

  return null;
};

export const getEventColor = () => {
  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return '#ec7337';
  }

  if (isHolidays()) {
    return '#ff3b4e';
  }

  return null;
};

export const getEventFont = () => {
  return null;
};

export const getEventText = () => {
  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return 'spookout';
  }

  if (currentDate.month() === 3 && currentDate.date() === 1 && currentDate.year() === 2021) {
    return 'amogus';
  }

  return 'knockout';
};

export const getEventQuotes = (flavor) => {
  if (flavor === PUNCHY_FLAVORS.UKRAINE) {
    return UKRAINE_QUOTES;
  }

  if (flavor === PUNCHY_FLAVORS.TRANS_RIGHTS) {
    return TRANS_RIGHTS_QUOTES;
  }

  if (isHolidays()) {
    return HOLIDAY_QUOTES;
  }

  return null;
};
