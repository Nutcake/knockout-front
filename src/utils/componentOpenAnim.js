const componentOpenAnim = (
  componentVisible,
  setComponentVisible,
  setOpenAnim,
  animationTransition
) => {
  if (!componentVisible) {
    setComponentVisible(true);
    setTimeout(() => {
      setOpenAnim(true);
    }, 10);
  } else {
    setOpenAnim(false);
    setTimeout(() => {
      setComponentVisible(false);
    }, animationTransition);
  }
};

export default componentOpenAnim;
