export const BAN_INFO_STORAGE_KEY = 'banInformation-v2';

export const clearBannedMessageFromStorage = () => {
  localStorage.removeItem(BAN_INFO_STORAGE_KEY);
};

export const loadBannedMessageFromStorage = () => {
  const banInformation = JSON.parse(localStorage.getItem(BAN_INFO_STORAGE_KEY));

  if (banInformation) {
    // if ban has not expired, return ban info; otherwise clear from storage
    const now = new Date();
    const expireDate = new Date(banInformation.expiresAt);
    if (expireDate > now) {
      return banInformation;
    }
    clearBannedMessageFromStorage();
  }
  return null;
};

export const saveBannedMessageToStorage = (data) => {
  const { banMessage, expiresAt, postId } = data;
  const banInformation = JSON.stringify({ banMessage, expiresAt, postId });

  localStorage.setItem(BAN_INFO_STORAGE_KEY, banInformation);
};
