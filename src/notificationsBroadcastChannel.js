const channel = typeof BroadcastChannel === 'function' ? new BroadcastChannel('notifications') : {};
export default channel;
