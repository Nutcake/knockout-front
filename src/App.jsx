import { loadProgressBar } from 'axios-progress-bar';
import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import Modal from 'react-modal';
import MainView from './views/MainView';
import store from './state/configureStore';
import AppThemeProvider from './AppThemeProvider';
import 'react-datetime-picker/dist/DateTimePicker.css';
import 'react-calendar/dist/Calendar.css';
import 'react-clock/dist/Clock.css';

// adds the axios progress bar to the global axios instance
loadProgressBar({
  parent: '#knockout-header',
});
toast.configure();

Modal.setAppElement('#app');

// /* Only register a service worker if it's supported */
// if ('serviceWorker' in navigator) {
//   window.addEventListener('load', () => {
//     navigator.serviceWorker
//       .register('./service-worker.js')
//       .then((registration) => {
//         console.log('SW registered: ', registration);
//       })
//       .catch((registrationError) => {
//         console.log('SW registration failed: ', registrationError);
//       });
//   });
// }

const App = () => {
  return (
    <Provider store={store}>
      <AppThemeProvider>
        <BrowserRouter basename="/">
          <MainView />
        </BrowserRouter>
      </AppThemeProvider>
    </Provider>
  );
};

export default App;

const container = document.getElementById('app');
const root = createRoot(container);
root.render(<App />);
