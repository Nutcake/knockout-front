import { HEADER_UPDATE, MOTD_UPDATE, THEME_UPDATE, WIDTH_UPDATE } from './actions';
import {
  loadStickyHeaderFromStorageBoolean,
  loadThemeFromStorage,
  loadWidthFromStorage,
} from '../../services/theme';

const initialState = {
  stickyHeader: loadStickyHeaderFromStorageBoolean(),
  theme: loadThemeFromStorage(),
  width: loadWidthFromStorage(),
  motd: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case HEADER_UPDATE:
      return { ...state, stickyHeader: action.value };
    case THEME_UPDATE:
      return { ...state, theme: action.value };
    case WIDTH_UPDATE:
      return { ...state, width: action.value };
    case MOTD_UPDATE:
      return { ...state, motd: action.value };
    default:
      break;
  }
  return state;
}
