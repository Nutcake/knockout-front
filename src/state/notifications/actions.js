import notificationsBroadcastChannel from '../../notificationsBroadcastChannel';

export const NOTIFICATION_SET = 'NOTIFICATION_SET';
export const NOTIFICATION_ADD = 'NOTIFICATION_ADD';
export const NOTIFICATION_READ = 'NOTIFICATION_READ';
export const NOTIFICATION_READ_ALL = 'NOTIFICATION_READ_ALL';

export function addNotification(value) {
  return {
    type: NOTIFICATION_ADD,
    value,
  };
}

export function setNotifications(value) {
  return {
    type: NOTIFICATION_SET,
    value,
  };
}

export function readNotification(value, sendMessage = true) {
  const action = {
    type: NOTIFICATION_READ,
    value,
  };
  if (sendMessage) {
    notificationsBroadcastChannel.postMessage(action);
  }
  return action;
}

export function readAllNotifications(sendMessage = true) {
  const action = {
    type: NOTIFICATION_READ_ALL,
  };
  if (sendMessage) {
    notificationsBroadcastChannel.postMessage(action);
  }
  return action;
}
