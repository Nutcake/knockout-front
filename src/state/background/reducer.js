import { BACKGROUND_UPDATE } from './actions';

const initialState = {
  url: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case BACKGROUND_UPDATE:
      return { ...state, url: action.value, type: action.bgType || 'cover' };
    default:
      break;
  }
  return state;
}
