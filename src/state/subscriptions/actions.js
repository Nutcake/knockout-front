import subscriptionsBroadcastChannel from '../../subscriptionsBroadcastChannel';

export const SUBSCRIPTION_SET_STATE = 'SUBSCRIPTION_SET_STATE';
export const SUBSCRIPTION_UPDATE_THREAD = 'SUBSCRIPTION_UPDATE_THREAD';
export const SUBSCRIPTION_REMOVE_THREAD = 'SUBSCRIPTION_REMOVE_THREAD';
export const SUBSCRIPTION_ADD_THREAD = 'SUBSCRIPTION_ADD_THREAD';

export function setSubscriptions(value) {
  return {
    type: SUBSCRIPTION_SET_STATE,
    value,
  };
}

export function addSubscriptionThread(value, sendMessage = true) {
  const action = {
    type: SUBSCRIPTION_ADD_THREAD,
    value,
  };
  if (sendMessage) {
    subscriptionsBroadcastChannel.postMessage(action);
  }
  return action;
}

export function updateSubscriptionThread(value, sendMessage = true) {
  const action = {
    type: SUBSCRIPTION_UPDATE_THREAD,
    value,
  };
  if (sendMessage) {
    subscriptionsBroadcastChannel.postMessage(action);
  }
  return action;
}

export function removeSubscriptionThread(value) {
  const action = {
    type: SUBSCRIPTION_REMOVE_THREAD,
    value,
  };
  subscriptionsBroadcastChannel.postMessage(action);
  return action;
}
