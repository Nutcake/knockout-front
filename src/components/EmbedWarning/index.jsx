import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { saveEmbedSettingToStorage } from '../../utils/thirdPartyEmbedStorage';
import {
  ThemeMainBackgroundColor,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';

export const StyledEmbedWarning = styled.div`
  width: 100%;
  background: ${ThemeMainBackgroundColor};
  color: ${ThemeTextColor};
  text-align: center;
  max-width: 450px;
  padding: calc(${ThemeVerticalPadding} * 2);
  box-sizing: border-box;
  border-radius: 5px;
  margin-bottom: 15px;

  button {
    border: none;
    background: #ffc107;
    padding: 10px;
    border-radius: 3px;
    cursor: pointer;
    margin-left: 5px;
    margin-right: 5px;

    &:hover,
    &:focus {
      filter: brightness(1.1);
    }
  }
`;

const EmbedWarning = ({ service, href }) => {
  /* eslint-disable react/jsx-one-expression-per-line, prettier/prettier */
  return (
    <StyledEmbedWarning>
      <img src="https://cdn.knockout.chat/assets/warning.png" alt="Warning icon" />
      <p>
        <b>Watch out!</b>
      </p>
      <p>{service} might track your data or do something else - it&apos;s out of our hands.</p>
      <p>
        You can <b>permanently </b>
        unlock 3rd party embeds <b>at your own risk</b> or go directly to the site.
      </p>
      <button onClick={() => saveEmbedSettingToStorage()} type="button">
        Unlock embeds
      </button>
      <a href={href} target="_blank" rel="noopener">
        <button type="button">
          Go to {service}
        </button>
      </a>
    </StyledEmbedWarning>
  );
  /* eslint-enable react/jsx-one-expression-per-line, prettier/prettier */
};

EmbedWarning.propTypes = {
  service: PropTypes.string,
  href: PropTypes.string,
};
EmbedWarning.defaultProps = {
  service: undefined,
  href: undefined,
};

export default EmbedWarning;
