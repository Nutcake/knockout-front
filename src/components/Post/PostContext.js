import { createContext } from 'react';

export default createContext({
  postEdited: new Date(2020, 1, 1).toISOString(),
  user: {
    createdAt: new Date(2020, 1, 1).toISOString(),
    posts: 20,
  },
  profileView: false,
});
