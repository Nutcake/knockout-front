import React from 'react';
import PropTypes from 'prop-types';

const BlankSlate = (props) => {
  const { resourceNamePlural } = props;

  return (
    <div>
      <p>Sorry, no {resourceNamePlural} were found.</p>
    </div>
  );
};

BlankSlate.propTypes = {
  resourceNamePlural: PropTypes.string.isRequired,
};

export default BlankSlate;
