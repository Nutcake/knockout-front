import React from 'react';
import styled from 'styled-components';
import config from '../../../config';

interface StyledLoginButtonProps {
  background?: string;
  color?: string;
}

export const StyledLoginButton = styled.button<StyledLoginButtonProps>`
  display: block;
  position: relative;
  background: ${(props) => props.background || 'black'};
  color: ${(props) => props.color || 'white'};
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  box-sizing: border-box;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

interface LoginButtonProps {
  background?: string;
  color?: string;
  route: string;
  children: React.ReactNode;
  newWindow?: boolean;
}

const LoginButton = ({
  background = undefined,
  color = undefined,
  route,
  children,
  newWindow = true,
}: LoginButtonProps) => (
  <StyledLoginButton
    color={color}
    background={background}
    as={newWindow ? undefined : 'a'}
    href={`${config.apiHost}${route}`}
    onClick={() =>
      newWindow &&
      window.open(
        `${config.apiHost}${route}`,
        newWindow ? '_blank' : undefined,
        'width=500,height=600,rel=opener'
      )
    }
  >
    {children}
  </StyledLoginButton>
);

export default LoginButton;
