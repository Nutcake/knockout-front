/* eslint-disable react/forbid-prop-types */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Modal from '../Modals/Modal';
import ModalSelect from '../Modals/ModalSelect';
import submitBan from '../../services/ban';
import {
  ThemeTextColor,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeVerticalPadding,
  ThemeFontFamily,
} from '../../utils/ThemeNew';
import { FieldLabelSmall, TextField } from '../FormControls';

const BAN_PERIODS = [
  {
    text: 'Hours',
    value: 'hours',
  },
  {
    text: 'Days',
    value: 'days',
  },
  {
    text: 'Months',
    value: 'months',
  },
  {
    text: 'Forever',
    value: 'forever',
  },
];

const computeBanLength = (length, period) => {
  let totalLength = length;
  switch (period) {
    case BAN_PERIODS[1].value: {
      totalLength *= 24;
      break;
    }
    case BAN_PERIODS[2].value: {
      totalLength *= 720;
      break;
    }
    case BAN_PERIODS[3].value: {
      totalLength = 0;
      break;
    }
    default: {
      // noop
    }
  }
  return totalLength;
};

const BanLength = styled.div`
  .ban-length {
    display: block;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeFontSizeMedium};
    font-family: ${ThemeFontFamily};
    line-height: 1.1;
    border: none;
    width: 100%;
    box-sizing: border-box;
    background: ${ThemeBackgroundLighter};
  }

  .ban-period {
    margin-top: ${ThemeVerticalPadding};
    background: ${ThemeBackgroundLighter};
  }
`;

const BanModal = ({ userId, postId, submitFn, cancelFn, isOpen }) => {
  const [banReason, setBanReason] = useState('');
  const [banLength, setBanLength] = useState(1);
  const [banPeriod, setBanPeriod] = useState(BAN_PERIODS[0].value);
  const [disableSubmit, setDisableSubmit] = useState(false);

  const banUser = async () => {
    await submitBan({
      userId,
      postId,
      banReason,
      banLength: computeBanLength(banLength, banPeriod),
    });
  };

  useEffect(() => {
    setBanPeriod(BAN_PERIODS[0].value);
  }, [isOpen]);

  useEffect(() => {
    setDisableSubmit(banReason.length < 1 || banLength < 1);
  }, [banReason, banLength]);

  return (
    <Modal
      iconUrl="/static/icons/siren.png"
      title="Ban User"
      cancelFn={cancelFn}
      submitFn={async () => {
        setDisableSubmit(true);
        await banUser();
        submitFn();
        setDisableSubmit(false);
      }}
      isOpen={isOpen}
      disableSubmit={disableSubmit}
    >
      <FieldLabelSmall>Ban Reason</FieldLabelSmall>
      <TextField
        aria-label="ban-reason"
        value={banReason}
        maxLength={250}
        onChange={(e) => setBanReason(e.target.value)}
      />
      <FieldLabelSmall>Ban Length</FieldLabelSmall>
      <BanLength>
        {banPeriod !== 'forever' && (
          <input
            aria-label="ban-length"
            className="ban-length"
            type="number"
            min="1"
            value={banLength}
            onChange={(e) => setBanLength(e.target.value)}
          />
        )}
        <ModalSelect
          className="ban-period"
          options={BAN_PERIODS}
          onChange={(e) => setBanPeriod(e.target.value)}
        />
      </BanLength>
    </Modal>
  );
};

export default BanModal;

BanModal.propTypes = {
  userId: PropTypes.number.isRequired,
  postId: PropTypes.number,
  submitFn: PropTypes.func.isRequired,
  cancelFn: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

BanModal.defaultProps = {
  postId: undefined,
};
