import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Rule } from 'knockout-schema';
import RuleCard from './components/RuleCard';

const StyledRules = styled.div`
  .rules-list {
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    width: 100%;
    padding: 0;
    list-style: none;
  }

  a {
    text-decoration: underline;
  }
`;

interface RulesProps {
  getRules: () => Promise<Rule[]>;
  interactable?: boolean;
  resource?: string;
}

const Rules = ({ getRules, interactable = true, resource = '' }: RulesProps) => {
  const selectedId = new URLSearchParams(useLocation().search).get('id');

  const [rulesLoading, setRulesLoading] = useState(true);
  const [rules, setRules] = useState<Rule[]>([]);

  useEffect(() => {
    setRulesLoading(true);
    const fetchRules = async () => {
      try {
        setRules(await getRules());
      } catch (error) {
        console.error(error);
      } finally {
        setRulesLoading(false);
      }
    };
    fetchRules();
  }, [getRules]);

  let rulesContent;

  if (rulesLoading) {
    rulesContent = <p>Loading the latest rules...</p>;
  } else if (rules.length === 0) {
    rulesContent = (
      <p>
        No {`${resource !== '' && resource}`} rules found. Please visit{' '}
        <a href="/rules" title="Knockout Rules">
          the Knockout Rules page
        </a>{' '}
        for general site rules.
      </p>
    );
  } else {
    rulesContent = (
      <ol className="rules-list">
        {rules.map((rule, i) => {
          const identifier = `d-${i + 1}`;
          const selected = identifier === selectedId;
          return (
            <RuleCard
              title={rule.title}
              key={identifier}
              cardinality={rule.cardinality}
              identifier={identifier}
              selected={selected}
              interactable={interactable}
            >
              {rule.description}
            </RuleCard>
          );
        })}
      </ol>
    );
  }

  return (
    <StyledRules>
      <Helmet>
        <title>Rules - Knockout!</title>
      </Helmet>
      <h1 className="underline">{resource !== '' ? `${resource} Rules` : `Rules`}</h1>
      <p className="detail">
        There are times where moderators will use their discretion and common sense to maintain a
        neutral, unbiased and fair stance. Think we’re not doing our job? Problem with a mute? You
        can contact us through the messaging system. Remember. Mutes are warnings. You are more than
        welcome to get an explanation if you don’t understand the warning.
      </p>
      {rulesContent}
    </StyledRules>
  );
};

export default Rules;
