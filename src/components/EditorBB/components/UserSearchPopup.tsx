import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import MiniUserInfo from '../../MiniUserInfo';
import { searchUsers } from '../../../services/user';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeHorizontalPadding,
  ThemeHoverShadow,
} from '../../../utils/ThemeNew';
import { getMentionQuery } from '../helpers/mention';

export const StyledUserSearchPopup = styled.div`
  max-height: 200px;
  width: max-content;
  overflow: auto;
  position: absolute;
  background-color: ${ThemeBackgroundDarker};
  padding: 15px;
  bottom: 0;
  box-shadow: ${ThemeHoverShadow};
  .result {
    display: flex;
    align-items: center;
    width: 100%;
    height: 40px;
    box-sizing: border-box;
    padding: 0 calc(${ThemeHorizontalPadding} * 1.5);
    cursor: pointer;

    &:hover {
      background: ${ThemeBackgroundLighter};
    }
  }

  scrollbar-width: thin;
  scrollbar-color: ${ThemeBackgroundLighter} transparent;
  &::-webkit-scrollbar-track {
    background-color: ${ThemeBackgroundDarker};
  }

  &::-webkit-scrollbar {
    width: 6px;
    height: 10px;
    background-color: ${ThemeBackgroundLighter};
  }

  &::-webkit-scrollbar-thumb {
    opacity: 0.5;
    background-color: ${ThemeBackgroundLighter};
  }
`;

interface UserSearchPopupProps {
  closePopup: () => void;
  caretPosition: number;
  content: string;
  onSizeChange: (size: { width: number; height: number }) => void;
  selectUser: (user: unknown) => void;
}

const UserSearchPopup = ({
  closePopup,
  selectUser,
  onSizeChange,
  caretPosition,
  content,
}: UserSearchPopupProps) => {
  const [searchResults, setSearchResults] = useState([]);
  const timer = useRef<NodeJS.Timeout | undefined>(undefined);
  const [loading, setLoading] = useState(false);
  const containerRef = useRef(null);
  const query = getMentionQuery(content, caretPosition);

  useEffect(() => {
    clearTimeout(timer.current);
    if (query) {
      timer.current = setTimeout(async () => {
        setLoading(true);
        const results = await searchUsers(query);
        setSearchResults(results);
        setLoading(false);
      }, 300);
    } else {
      setSearchResults([]);
    }
  }, [query]);

  useEffect(() => {
    if (containerRef.current && onSizeChange) {
      const width = containerRef.current.clientWidth;
      const height = containerRef.current.clientHeight;
      onSizeChange({ width, height });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchResults]);

  useEffect(() => {
    document.addEventListener('click', closePopup);
    return () => {
      document.removeEventListener('click', closePopup);
    };
  }, [closePopup]);

  if (query.length === 0 || (!loading && searchResults.length === 0)) {
    return null;
  }

  return (
    <StyledUserSearchPopup ref={containerRef}>
      {loading && <div>Loading...</div>}
      {!loading &&
        searchResults.map((result) => {
          return (
            <div
              key={result.id}
              className="result"
              role="button"
              tabIndex={0}
              onClick={() => {
                selectUser(result);
                closePopup();
              }}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  selectUser(result);
                  closePopup();
                }
              }}
            >
              <MiniUserInfo as="div" user={result} defaultAvatar />
            </div>
          );
        })}
    </StyledUserSearchPopup>
  );
};

export default UserSearchPopup;
