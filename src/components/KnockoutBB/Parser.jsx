/* eslint-disable no-use-before-define */
import React from 'react';
import { ShortcodeTree, ShortcodeNode, TextNode } from 'shortcode-tree';

import styled from 'styled-components';
import { transparentize } from 'polished';
import Schema from './Schema';
import { ThemePostLineHeight, ThemeVerticalPadding } from '../../utils/ThemeNew';
import UserRoleWrapper from '../UserRoleWrapper';
import userRoles from '../UserRoleWrapper/userRoles';

const UNPARSED_TAGS = ['code', 'noparse'];
const NON_BLOCK_TAGS = ['b', 'i', 'u', 's', 'url', 'spoiler', 'noparse', 'img', 'video'];

const StyledParser = styled.div`
  padding: ${ThemeVerticalPadding} 0;
  line-height: ${ThemePostLineHeight}%;
`;

const StyledMention = styled.a`
  background: ${(props) => transparentize(0.9, props.roleColor(props))};
  padding: 1.5px 2px;
  margin-right: 2px;
  font-weight: bold;

  &:hover {
    background: ${(props) =>
      transparentize(props.theme.mode === 'light' ? 0 : 0.7, props.roleColor(props))};
    ${(props) =>
      props.theme.mode === 'light' ? ` .user-role-wrapper-component { color: white; }` : ''}
  }
`;

// Returns if the node should ignore newlines
// directly after the node's closing tag.
function shouldCheckNewLine(node) {
  return node instanceof ShortcodeNode && !NON_BLOCK_TAGS.includes(node.shortcode.name);
}

const MENTION_REGEX = /@<(\d+;?.*)>/g;
const MENTION_GROUP_REGEX = /@<(\d+;.*?)>/g;

const replaceMentionsWithLinks = (content) => {
  if (!content) return content;

  const splitContent = content.split(MENTION_GROUP_REGEX);
  const mentions = content.match(MENTION_GROUP_REGEX);

  if (splitContent.length <= 1) {
    return content;
  }

  return splitContent.reduce((arr, element) => {
    if (!element) return arr;

    const contentMention = mentions.find((mention) => mention.includes(element));
    if (contentMention) {
      const components = contentMention.split(MENTION_REGEX);
      if (components.length < 2) {
        return [...arr, element];
      }
      const userInfo = components[1].split(';');
      const userId = userInfo[0];
      const username = userInfo[1];
      const roleCode = userInfo[2];
      const role = userRoles[roleCode] ?? userRoles['basic-user'];
      const roleColor = typeof role.color === 'function' ? role.color : () => role.color;
      return [
        ...arr,
        <StyledMention key={userId} href={`/user/${userId}`} roleColor={roleColor}>
          <UserRoleWrapper key={userId} user={{ id: userId, username, role: { code: roleCode } }}>
            {`@${username}`}
          </UserRoleWrapper>
        </StyledMention>,
      ];
    }

    return [...arr, element];
  }, []);
};

function parseTree(node, index, checkNewLine = false, spoiler = false) {
  if (node instanceof TextNode || (node instanceof ShortcodeNode && node.shortcode === null)) {
    const trimNewLine = checkNewLine && node.text && node.text[0] === '\n';
    const textWithMentionLinks = trimNewLine
      ? replaceMentionsWithLinks(node.text.substring(1))
      : replaceMentionsWithLinks(node.text);
    return <span key={`${node.text}-${index}`}>{textWithMentionLinks}</span>;
  }

  if (node instanceof ShortcodeNode) {
    const { properties } = node.shortcode;
    const tag = node.shortcode.name;
    // return just the text contents of tags in UNPARSED_TAGS. prevents breakage
    if (UNPARSED_TAGS.includes(tag)) {
      return (
        <Schema
          key={`${tag}-${index}`}
          tag={tag}
          content={node.text}
          properties={{ ...properties, key: `${tag}-${index}` }}
          raw={node.shortcode.codeText}
        />
      );
    }

    // dont mention in spoiler tags
    let content = node.text;

    if (tag === 'spoiler') {
      content = replaceMentionsWithLinks(node.text);
    }

    if (node.children.length === 0 && node.text) {
      return (
        <Schema
          key={`${tag}-${index}`}
          tag={tag}
          content={content}
          properties={{ ...properties, key: `${tag}-${index}` }}
          raw={node.shortcode.codeText}
          spoiler={spoiler}
        />
      );
    }

    content = (
      <>
        {node.children.map((child, i) =>
          parseTree(child, i, i > 0 && shouldCheckNewLine(node.children[i - 1]), tag === 'spoiler')
        )}
      </>
    );

    return (
      <Schema
        key={`${tag}-${index}`}
        tag={tag}
        content={content}
        properties={{ ...properties, key: `${tag}-${index}` }}
        raw={node.shortcode.codeText}
        hasChildren
      />
    );
  }

  return null;
}

export const bbToTree = (string) => ShortcodeTree.parse(string);

const Parser = ({ content }) => {
  try {
    const shortCodes = ShortcodeTree.parse(content);

    const comps =
      shortCodes.children.length > 0
        ? shortCodes.children.map((node, index) =>
            parseTree(node, index, index > 0 && shouldCheckNewLine(shortCodes.children[index - 1]))
          )
        : parseTree(shortCodes);

    return <StyledParser>{comps}</StyledParser>;
  } catch (error) {
    console.error(error);
    return <pre>Could not parse BBcode.</pre>;
  }
};
export default Parser;
