import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Modal from '../../Modals/Modal';

const StyledSpoilerButton = styled.button`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 73px;
  height: 73px;
  outline: none;
  border: none;
  background: black;
  border-radius: 50%;
  color: white;
  font-size: 30px;
  padding: 18px;
  z-index: 1;
  opacity: 0.5;
  transition: 0.4s;
  cursor: pointer;
  &:hover {
    opacity: 0.9;
  }
`;

const SpoilerButton = ({ reveal, limitedUser, setSpoilered, contentName }) => {
  const [modalOpen, setModalOpen] = useState(false);

  return (
    <>
      {limitedUser && (
        <Modal
          title={`View ${contentName}?`}
          cancelFn={() => setModalOpen(false)}
          submitFn={() => {
            setSpoilered(false);
            setModalOpen(false);
          }}
          submitText="View"
          isOpen={modalOpen}
        >
          {`This ${contentName} was posted by a recently created or inactive account, and may contain
        explicit imagery. Are you sure you want to view this ${contentName}?`}
        </Modal>
      )}
      <StyledSpoilerButton
        className="spoiler-button"
        title="View image"
        type="button"
        onClick={() => reveal(setModalOpen)}
      >
        <i className={limitedUser ? 'fa-solid fa-user-alt-slash' : 'far fa-eye-slash'} />
      </StyledSpoilerButton>
    </>
  );
};

SpoilerButton.propTypes = {
  reveal: PropTypes.func.isRequired,
  limitedUser: PropTypes.bool.isRequired,
  setSpoilered: PropTypes.func.isRequired,
  contentName: PropTypes.string.isRequired,
};

export default SpoilerButton;
