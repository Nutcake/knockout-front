import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { ThemeTextColor } from '../../../utils/ThemeNew';

const StyledSpoiler = styled.span`
  ${(props) => !props.hasChildNodes && `background: ${ThemeTextColor(props)};`}
  color: ${ThemeTextColor};

  span {
    background: ${ThemeTextColor};
  }

  a {
    color: ${ThemeTextColor};
  }

  &:hover {
    background: transparent;
  }
`;

const SpoilerBB = ({ children, hasChildNodes }) => (
  <StyledSpoiler hasChildNodes={hasChildNodes}>{children}</StyledSpoiler>
);

SpoilerBB.propTypes = {
  children: PropTypes.node.isRequired,
  hasChildNodes: PropTypes.bool,
};

SpoilerBB.defaultProps = {
  hasChildNodes: false,
};

export default SpoilerBB;
