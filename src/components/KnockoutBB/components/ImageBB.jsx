import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import SpoilerButton from './SpoilerButton';
import useSpoiler from './useLimitedSpoiler';

/**
 * A styled image block component.
 *
 * @type {Component}
 */
export const StyledImage = styled.div`
  position: relative;
  display: inline-block;
  overflow: hidden;

  .image {
    display: inline-block;
    max-width: 100%;
    max-height: 100vh;

    ${(props) => props.spoiler && 'filter: blur(45px);'}
    ${(props) => props.spoiler && 'pointer-events: none;'}
    ${(props) => props.isLink && `border: 1px dotted gray;`}

    ${(props) =>
      props.isThumbnail &&
      `max-width: 300px;
      max-height: 300px;`}
  }
`;

/*
 * A function to determine whether a URL has an image extension.
 *
 * @param {String} url
 * @return {Boolean}
 */
export const isImage = (url) => {
  const imageExtensions = ['png', 'jpg', 'jpeg', 'gif', 'webp'];
  const rgx = /(?:\.([^.]+))?$/;

  const extension = rgx.exec(url.toLowerCase());

  return !!imageExtensions.includes(extension[1]);
};

const ImageBB = ({ href, thumbnail, link, spoiler }) => {
  const [spoilered, setSpoilered, reveal, limitedUser] = useSpoiler(spoiler);

  const button = spoilered && (
    <SpoilerButton
      reveal={reveal}
      limitedUser={limitedUser}
      setSpoilered={setSpoilered}
      contentName="image"
    />
  );

  const image = <img className="image" src={href} alt="post embed" />;

  let styledImage = (
    <StyledImage spoiler={spoilered}>
      {button}
      {image}
    </StyledImage>
  );

  if (link || thumbnail) {
    styledImage = (
      <StyledImage src={href} isThumbnail={thumbnail} isLink={link} spoiler={spoilered}>
        {button}
        <a href={href} target="_blank" rel="noopener">
          {image}
        </a>
      </StyledImage>
    );
  }

  return styledImage;
};

ImageBB.propTypes = {
  href: PropTypes.string.isRequired,
  thumbnail: PropTypes.bool.isRequired,
  link: PropTypes.bool.isRequired,
  spoiler: PropTypes.bool,
};

ImageBB.defaultProps = {
  spoiler: false,
};

export default ImageBB;
