import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';

export const isReddit = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'www.reddit.com') throw new Error();
    return true;
  } catch (error) {
    return null;
  }
};

export const useAsyncScript = (src, id = '', properties = []) => {
  useEffect(() => {
    let script;
    if (!id || !document.getElementById(id)) {
      const first = document.getElementsByTagName('script')[0];
      script = document.createElement('script');
      script.id = id;
      script.src = src;
      script.async = true;
      first.parentNode.insertBefore(script, first);
    }
    return () => {
      if (script) document.head.removeChild(script);
    };
  }, [src, ...properties]);
};

const RedditBB = ({ href, children }) => {
  const url = new URL(href || children.join(''));
  const theme = useContext(ThemeContext);

  useAsyncScript('https://embed.reddit.com/widgets.js');

  return (
    <blockquote
      className="reddit-embed-bq"
      style={{ height: '500px' }}
      data-embed-theme={theme.mode !== 'light' ? 'dark' : ''}
      data-embed-height="500"
      loading="lazy"
    >
      <a href={url}>Reddit embed</a>
    </blockquote>
  );
};

RedditBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

export default RedditBB;
