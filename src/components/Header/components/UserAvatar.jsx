import React from 'react';
import PropTypes from 'prop-types';
import config from '../../../../config';

const UserAvatar = ({ user, defaultUrl, className }) => {
  const hasAvatar =
    user && user.avatarUrl && user.avatarUrl.length !== 0 && !user.avatarUrl.includes('none.webp');
  let url = defaultUrl;
  if (hasAvatar) {
    url = `${config.cdnHost}/image/${user.avatarUrl}`;
  }
  if (user && user.isBanned) {
    url = 'https://img.icons8.com/color/80/000000/minus.png';
  }

  return (
    <img
      className={className}
      src={url}
      title={`${user ? `${user.username}'s ` : ''}avatar`}
      alt=""
      onError={(e) => {
        e.target.onerror = null;
        e.target.src = `${config.cdnHost}/image/none.webp`;
      }}
    />
  );
};

UserAvatar.propTypes = {
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string,
    isBanned: PropTypes.bool,
  }),
  defaultUrl: PropTypes.string,
  className: PropTypes.string,
};

UserAvatar.defaultProps = {
  user: undefined,
  defaultUrl: `${config.cdnHost}/image/none.webp`,
  className: '',
};

export default UserAvatar;
