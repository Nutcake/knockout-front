import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import humanizeDuration from '../../../utils/humanizeDuration';
import {
  ThemeBannedUserColor,
  ThemeFontSizeHeadline,
  ThemeHighlightStronger,
} from '../../../utils/ThemeNew';
import banIsPermaban from '../../../utils/banIsPermaban';
import { getPost } from '../../../services/posts';

export const BanInfoWrapper = styled.div`
  width: 100%;
  text-align: center;
  min-height: 50px;
  line-height: 25px;
  background: ${(props) => (props.isPermaban ? ThemeBannedUserColor : ThemeHighlightStronger)};
  padding: 10px;
  margin-bottom: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  color: white;

  .ban-info-header {
    font-size: ${ThemeFontSizeHeadline};
    font-weight: bold;
    margin-bottom: 10px;
  }

  .ban-info-post {
    display: inline;
  }

  .post-preview {
    font-weight: bold;
  }
`;

export const BanInfoIcon = styled.img`
  width: 50px;
  height: 50px;
  margin-right: 15px;
`;

export const BanInfoText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 100%;
  min-height: 40px;
`;

export const BanInfoStrong = styled(Link)`
  font-weight: bold;
  text-decoration: underline;
`;

const BannedHeaderMessage = ({ banMessage, expiresAt, postId }) => {
  const now = new Date();
  const expiresAtDate = Date.parse(expiresAt);
  const isPermaban = banIsPermaban(expiresAtDate);
  const duration = humanizeDuration(now, expiresAtDate);

  const [post, setPost] = useState(null);

  useEffect(() => {
    async function loadPost() {
      try {
        const fetchedPost = await getPost(postId);
        setPost(fetchedPost.data);
      } catch (e) {
        console.error(`Could not fetch post: ${e}`);
      }
    }

    if (postId) {
      loadPost();
    }
  }, [postId]);

  const headerMessage = `You've been ${isPermaban ? 'Banned' : 'Muted'}`;
  const altText = isPermaban ? 'Banned' : 'Muted';

  return (
    <BanInfoWrapper isPermaban={isPermaban}>
      <BanInfoIcon
        src="https://img.icons8.com/color/50/000000/police-badge.png"
        alt={altText}
        title={altText}
      />
      <div>
        <div className="ban-info-header">{headerMessage}</div>
        {post && (
          <div className="ban-info-post">
            for&nbsp;
            <BanInfoStrong to={`/thread/${post.thread}/${post.page}#post-${postId}`}>
              this post
            </BanInfoStrong>
          </div>
        )}
        <span> with the reason: </span>
        <b>
          &quot;
          {banMessage}
          &quot;
        </b>
        <div>
          {isPermaban ? (
            <>You are banned forever.</>
          ) : (
            <>
              {`You will be unmuted in `}
              <b title={expiresAt}>{duration}</b>
            </>
          )}
        </div>
        For more info, or if you want to appeal this decision, please visit the&nbsp;
        <BanInfoStrong to="/subforum/18">Whack Ass Crystal Prison.</BanInfoStrong>
      </div>
      <BanInfoIcon
        src="https://img.icons8.com/color/50/000000/police-badge.png"
        alt={altText}
        title={altText}
      />
    </BanInfoWrapper>
  );
};

BannedHeaderMessage.propTypes = {
  banMessage: PropTypes.string.isRequired,
  expiresAt: PropTypes.string.isRequired,
  postId: PropTypes.string,
};

BannedHeaderMessage.defaultProps = {
  postId: '',
};

export default BannedHeaderMessage;
