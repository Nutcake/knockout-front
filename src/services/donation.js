import { authGet, authPost } from './common';

export const getDonationCheckoutSessionId = async (successUrl, cancelUrl) => {
  try {
    const result = await authPost({
      url: '/v2/donations/checkout-session',
      data: {
        successUrl,
        cancelUrl,
      },
    });
    return result.data.sessionId;
  } catch (err) {
    console.error(err);
    return null;
  }
};

export const getDonationUpgradeExpiration = async () => {
  try {
    const result = await authGet({
      url: '/v2/donations/upgrade-expiration',
    });
    return result.data.expiresAt;
  } catch (err) {
    console.error(err);
    return null;
  }
};
