import { authGet, authPost, authPut } from './common';

export const getConversations = async () => {
  const results = await authGet({ url: `/conversations` });
  return results.data;
};

export const sendMessage = async ({ receivingUserId, content, conversationId = undefined }) => {
  const requestBody = {
    receivingUserId,
    content,
    conversationId,
  };
  const results = await authPost({ url: `/messages`, data: requestBody });
  return results.data;
};

export const readMessage = async (id) => {
  const results = await authPut({ url: `/messages/${id}` });
  if (!results.data.readAt) {
    throw Error('Unable to mark message as read.');
  }
  return results.data;
};

export const getConversationMessages = async (id) => {
  const results = await authGet({ url: `/conversations/${id}` });
  return results.data.messages.reverse();
};
