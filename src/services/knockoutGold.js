import { authDelete, authGet, authPost } from './common';

export const getKnockoutGoldCheckoutSessionId = async (successUrl, cancelUrl) => {
  try {
    const result = await authPost({
      url: `/v2/gold-subscriptions/checkout-session`,
      data: {
        successUrl,
        cancelUrl,
      },
    });
    return result.data.sessionId;
  } catch (err) {
    console.error(err);
    return null;
  }
};

export const getKnockoutGoldSubscription = async () => {
  try {
    const result = await authGet({
      url: `/v2/gold-subscriptions`,
    });
    return result.data;
  } catch (err) {
    console.error(err);
    return null;
  }
};

export const cancelKnockoutGoldSubscription = async () => {
  try {
    const result = await authDelete({
      url: `/v2/gold-subscriptions`,
    });
    return result.data;
  } catch (err) {
    console.error(err);
    return null;
  }
};
