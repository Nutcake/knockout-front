import dayjs from 'dayjs';
import { authPost } from './common';
import { pushSmartNotification, pushNotification } from '../utils/notification';

const submitBan = async ({ userId, banLength, banReason, postId }) => {
  if (!userId || banLength === undefined || !banReason) return;

  const requestBody = {
    userId,
    banLength,
    banReason,
    postId,
  };

  const response = await authPost({ url: '/ban', data: requestBody });

  if (response.data.error) {
    pushSmartNotification({ error: response.data.error });
  } else {
    const expiration = dayjs(response.data.banExpiresAt);
    let message;
    if (banLength <= 0) {
      message = 'User banned forever.';
    } else {
      message = `User muted. The mute will be lifted at ${expiration.format('HH:mm DD/MM/YYYY')}`;
    }
    pushNotification({
      message,
      type: 'success',
    });
  }
};

export default submitBan;
