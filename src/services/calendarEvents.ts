import {
  CalendarEvent,
  CreateCalendarEventRequest,
  UpdateCalendarEventRequest,
  CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS,
} from 'knockout-schema';
import { authDelete, authGet, authPost, authPut } from './common';

interface UpdateCalendarEventParams extends UpdateCalendarEventRequest {
  id: number;
}

/**
 * Validates that starting and ending date pair is valid for a calendar event
 *
 * @param {Date} startDate the starting date; must be before the end date
 * @param {Date} endDate the end date; must not be more then CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS days after the start date
 *
 * @returns {boolean} if the dates are valid
 */
const validateDates = (startDate: Date, endDate: Date): boolean => {
  const msDiff = endDate.getTime() - startDate.getTime();

  return msDiff > 0 && msDiff / (1000 * 60 * 60 * 24) < CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS;
};

/**
 * Fetch the calendar events that start between two dates
 *
 * @param {Date} startDate the starting date of the query
 * @param {Date} endDate the ending date of the query
 *
 * @returns {Promise<Array<CalendarEvent>>} an array of calendar events
 */
export const getCalendarEvents = async (
  startDate: Date,
  endDate: Date
): Promise<Array<CalendarEvent>> => {
  if (!validateDates(startDate, endDate)) {
    console.error(
      `Start and End date range must be greater than 0 and less than ${CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS} days`
    );
    return [];
  }

  const startDateIso = startDate.toISOString();
  const endDateIso = endDate.toISOString();

  const results = await authGet({
    url: `/v2/calendarEvents?startDate=${startDateIso}&endDate=${endDateIso}`,
  });
  return results.data;
};

/**
 * Create a calendar event
 *
 * @param {Object} params the creation parameters
 * @param {string} params.title the title of the calendar event
 * @param {string} params.description the description of the calendar event
 * @param {number} params.threadId the thread ID of the calendar event
 * @param {Date} params.startsAt the starting date of the calendar event
 * @param {Date} params.endsAt the ending date of the calendar event
 *
 * @returns {Promise<CalendarEvent>} the created calendar event
 */
export const createCalendarEvent = async ({
  title,
  description,
  threadId,
  startsAt,
  endsAt,
}: CreateCalendarEventRequest): Promise<CalendarEvent | null> => {
  const startsAtDate = new Date(startsAt);
  const endsAtDate = new Date(endsAt);

  if (!validateDates(startsAtDate, endsAtDate)) {
    console.error(
      `Start and End date range must be greater than 0 and less than ${CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS} days`
    );
    return null;
  }

  const body: CreateCalendarEventRequest = {
    title,
    description,
    threadId,
    startsAt,
    endsAt,
  };

  const results = await authPost({
    url: '/v2/calendarEvents',
    data: body,
  });
  return results ? results.data : null;
};

/**
 * Update a calendar event
 *
 * @param {Object} params the update parameters
 * @param {number} params.id the ID of the calendar event to update
 * @param {string} params.title the title of the calendar event
 * @param {string} params.description the description of the calendar event
 * @param {number} params.threadId the thread ID of the calendar event
 * @param {Date} params.startsAt the starting date of the calendar event
 * @param {Date} params.endsAt the ending date of the calendar event
 *
 * @returns {Promise<CalendarEvent>} the updated calendar event
 */
export const updateCalendarEvent = async ({
  id,
  title,
  description,
  threadId,
  startsAt,
  endsAt,
}: UpdateCalendarEventParams): Promise<CalendarEvent | null> => {
  if (startsAt && endsAt) {
    const startsAtDate = new Date(startsAt);
    const endsAtDate = new Date(endsAt);

    if (!validateDates(startsAtDate, endsAtDate)) {
      console.error(
        `Start and End date range must be greater than 0 and less than ${CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS} days`
      );
      return null;
    }
  }

  const body: UpdateCalendarEventRequest = {
    title,
    description,
    threadId,
    startsAt,
    endsAt,
  };

  const results = await authPut({
    url: `/v2/calendarEvents/${id}`,
    data: body,
  });
  return results ? results.data : null;
};

/**
 * Delete a calendar event
 *
 * @param {number} id the ID of the calendar event to delete
 *
 * @returns {Promise<Object>} the deletion response
 */
export const deleteCalendarEvent = async (id: number): Promise<object> => {
  const results = await authDelete({ url: `/v2/calendarEvents/${id}`, data: {} });
  return results ? results.data : null;
};
