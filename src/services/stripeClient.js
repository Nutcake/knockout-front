import { loadStripe } from '@stripe/stripe-js';

export default async () => {
  return loadStripe(process.appConfig.stripePublishableKey);
};
