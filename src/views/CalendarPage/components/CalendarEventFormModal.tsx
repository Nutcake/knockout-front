import React, { useEffect, useState, useRef } from 'react';
import styled from 'styled-components';
import DateTimePicker from 'react-datetime-picker';
import { lighten, transparentize } from 'polished';
import { Link } from 'react-router-dom';
import {
  CalendarEvent,
  CreateCalendarEventRequest,
  CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS,
  Thread,
} from 'knockout-schema';
import Modal from '../../../components/Modals/Modal';
import {
  FieldLabel,
  FieldLabelSmall,
  TextField,
  TextFieldLarge,
} from '../../../components/FormControls';
import ForumIcon from '../../../components/ForumIcon';
import { threadSearch, ThreadSearchParams } from '../../../services/threadSearch';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontFamily,
  ThemeFontSizeHuge,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { useAppSelector } from '../../../state/hooks';

const TITLE_CHAR_MIN = 5;
const TITLE_CHAR_MAX = 255;
const DESCRIPTION_CHAR_MIN = 5;
const DESCRIPTION_CHAR_MAX = 5000;

const StyledModalContent = styled.div`
  .datepicker {
    font-family: ${ThemeFontFamily};
    color: ${ThemeTextColor};
    margin-bottom: calc(${ThemeVerticalPadding} * 2);

    .react-datetime-picker__inputGroup__input {
      color: ${ThemeTextColor};
    }

    .react-datetime-picker__calendar-button__icon {
      stroke: ${ThemeTextColor};
    }

    .react-datetime-picker__inputGroup__input {
      option {
        color: ${ThemeTextColor};
        background-color: ${ThemeBackgroundDarker};
      }
    }

    .calendar {
      font-family: ${ThemeFontFamily};
      margin-bottom: calc(${ThemeVerticalPadding} * 2);
      background-color: ${ThemeBackgroundLighter};
      overflow: visible;

      .react-calendar__navigation__arrow {
        color: ${ThemeTextColor};

        &:disabled {
          color: ${(props) => transparentize(0.5, ThemeTextColor(props))};
          background-color: ${ThemeBackgroundDarker};
        }

        &:hover:not(:disabled) {
          background-color: ${(props) => lighten(0.2, ThemeBackgroundLighter(props))};
        }
      }

      .react-calendar__navigation__label {
        color: ${ThemeTextColor};

        &:hover {
          background-color: ${(props) => lighten(0.2, ThemeBackgroundLighter(props))};
        }
      }

      .day {
        font-family: ${ThemeFontFamily};
        color: ${ThemeTextColor};

        &:disabled {
          color: ${(props) => transparentize(0.5, ThemeTextColor(props))};
          background-color: ${ThemeBackgroundDarker};
        }

        &:hover:not(:disabled) {
          background-color: ${(props) => lighten(0.2, ThemeBackgroundLighter(props))};
        }
      }
    }
  }

  .selected-thread {
    display: inline-block;
    margin-bottom: ${ThemeVerticalPadding};

    .thread-icon {
      width: ${ThemeFontSizeHuge};
      vertical-align: text-bottom;
      margin-right: calc(${ThemeHorizontalPadding} / 2);
    }

    .thread-title {
      vertical-align: text-top;
    }
  }

  .timezone {
    margin-top: 0;
  }

  .thread-search-results {
    max-height: 220px;
    overflow: auto;
    background: ${(props) => lighten(0.1, ThemeBackgroundDarker(props))};
    margin-bottom: ${ThemeVerticalPadding};

    .result {
      display: flex;
      overflow: hidden;
      font-size: ${ThemeFontSizeSmall};
      align-items: center;
      height: 50px;
      box-sizing: border-box;
      padding: 0;
      cursor: pointer;

      &:hover {
        background: ${ThemeBackgroundLighter};
      }

      .icon {
        width: ${ThemeFontSizeHuge};
        margin-left: calc(${ThemeHorizontalPadding} / 2);
        margin-right: calc(${ThemeHorizontalPadding} / 2);
      }
    }

    scrollbar-width: thin;
    scrollbar-color: ${ThemeBackgroundDarker} transparent;
    &::-webkit-scrollbar-track {
      background-color: ${ThemeBackgroundLighter};
    }

    &::-webkit-scrollbar {
      width: 6px;
      height: 10px;
      background-color: ${ThemeBackgroundDarker};
    }

    &::-webkit-scrollbar-thumb {
      opacity: 0.5;
      background-color: ${ThemeBackgroundDarker};
    }
  }
`;

interface CalendarEventFormModalProps {
  isOpen: boolean;
  onSubmit: (createParams: CreateCalendarEventRequest) => void;
  onClose: () => void;
  defaultDate: Date;
  action: string;
  event: CalendarEvent | null;
}

const CalendarEventFormModal = ({
  isOpen,
  onSubmit,
  onClose,
  defaultDate,
  action,
  event,
}: CalendarEventFormModalProps) => {
  const currentUser = useAppSelector((state) => state.user);

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [threadSearchQuery, setThreadSearchQuery] = useState('');
  const [threadSearchResults, setThreadSearchResults] = useState<Thread[]>([]);
  const threadSearchTimer = useRef<ReturnType<typeof setTimeout>>();
  const [thread, setThread] = useState<Thread | null>(null);
  const [submitDisabled, setSubmitDisabled] = useState(true);

  const defaultStartsAt = new Date(defaultDate);
  defaultStartsAt.setMinutes(0);

  const defaultEndsAt = new Date(defaultStartsAt);
  defaultEndsAt.setHours(defaultStartsAt.getHours() + 1);

  const [startsAt, setStartsAt] = useState(defaultStartsAt);
  const [endsAt, setEndsAt] = useState(defaultEndsAt);

  const restrictThreadSearch = ['gold-user', 'paid-gold-user'].includes(currentUser?.role?.code);

  useEffect(() => {
    clearTimeout(threadSearchTimer.current);
    if (threadSearchQuery?.length >= 5) {
      threadSearchTimer.current = setTimeout(async () => {
        const searchParams: ThreadSearchParams = { title: threadSearchQuery, sortBy: 'relevance' };
        if (restrictThreadSearch) {
          searchParams.userId = currentUser.id;
        }

        const searchResult = await threadSearch(searchParams);
        setThreadSearchResults(searchResult.threads);
      }, 600);
    } else {
      setThreadSearchResults([]);
    }
  }, [threadSearchQuery, currentUser.id, restrictThreadSearch]);

  // set startsAt and endsAt to default dates when it changes
  useEffect(() => {
    const newStartsAt = new Date(defaultDate);
    newStartsAt.setMinutes(0);

    const newEndsAt = new Date(newStartsAt);
    newEndsAt.setHours(newEndsAt.getHours() + 1);

    setStartsAt(newStartsAt);
    setEndsAt(newEndsAt);
  }, [defaultDate]);

  // make sure endsAt is after startsAt
  useEffect(() => {
    if (endsAt <= startsAt) {
      const newEndsAt = new Date(startsAt);
      newEndsAt.setHours(newEndsAt.getHours() + 1);
      setEndsAt(newEndsAt);
    }
  }, [startsAt, endsAt]);

  // set form data to event if one is present
  useEffect(() => {
    if (event) {
      setTitle(event.title);
      setDescription(event.description);
      setThread(event.thread);
      setStartsAt(new Date(event.startsAt));
      setEndsAt(new Date(event.endsAt));
    }
  }, [event]);

  useEffect(() => {
    const startsAtInPast = startsAt.getTime() < new Date().getTime();

    const submitEnabled =
      title.length >= TITLE_CHAR_MIN &&
      title.length <= TITLE_CHAR_MAX &&
      description.length >= DESCRIPTION_CHAR_MIN &&
      description.length <= DESCRIPTION_CHAR_MAX &&
      thread != null &&
      startsAt < endsAt &&
      !startsAtInPast;

    setSubmitDisabled(!submitEnabled);
  }, [title, description, thread, startsAt, endsAt]);

  const clearForm = () => {
    setTitle('');
    setDescription('');
    setThread(null);
    setThreadSearchQuery('');
    setThreadSearchResults([]);
    setStartsAt(defaultStartsAt);
    setEndsAt(defaultEndsAt);
  };

  const submit = () => {
    const threadId = thread?.id;
    if (!threadId) return;

    const startsAtISO = startsAt.toISOString();
    const endsAtISO = endsAt.toISOString();

    onSubmit({ title, description, threadId: thread.id, startsAt: startsAtISO, endsAt: endsAtISO });
    clearForm();
  };

  const close = () => {
    clearForm();
    onClose();
  };

  const maxEndsAt = new Date(startsAt);
  maxEndsAt.setDate(maxEndsAt.getDate() + CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS);

  const nearestHour = new Date(defaultDate);
  nearestHour.setMinutes(nearestHour.getMinutes() + 30);
  nearestHour.setMinutes(0);

  const isUpdating = action === 'update';

  return (
    <Modal
      iconUrl="/static/icons/planner.png"
      title={`${isUpdating ? 'Update' : 'Create'} Community Event`}
      cancelFn={close}
      submitFn={submit}
      submitText={action === 'update' ? 'Update' : 'Submit'}
      isOpen={isOpen}
      disableSubmit={submitDisabled}
    >
      <StyledModalContent>
        <FieldLabel>Title</FieldLabel>
        <FieldLabelSmall>{`Minimum ${TITLE_CHAR_MIN} characters`}</FieldLabelSmall>
        <TextField
          maxLength={TITLE_CHAR_MAX}
          placeholder="Knockout plays Among Us"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <FieldLabel>Description</FieldLabel>
        <FieldLabelSmall>{`Minimum ${DESCRIPTION_CHAR_MIN} characters`}</FieldLabelSmall>
        <TextFieldLarge
          maxLength={DESCRIPTION_CHAR_MAX}
          placeholder="Come join our Among Us session"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <FieldLabel>Thread</FieldLabel>
        {restrictThreadSearch && <FieldLabelSmall>Must be one of your threads</FieldLabelSmall>}
        {thread && (
          <Link className="selected-thread" to={`/thread/${thread.id}`} target="_blank">
            <ForumIcon iconId={thread.iconId} className="thread-icon" />
            <span className="thread-title">{thread.title}</span>
          </Link>
        )}
        <TextField
          placeholder={
            thread ? 'Search and select a new thread' : 'Search for a thread (minimum 5 characters)'
          }
          value={threadSearchQuery}
          onChange={(e) => setThreadSearchQuery(e.target.value)}
        />
        {threadSearchResults.length > 0 && (
          <div className="thread-search-results">
            {threadSearchResults.slice(0, 10).map((threadResult) => (
              <div
                key={threadResult.id}
                className="result"
                role="button"
                tabIndex={0}
                onClick={() => {
                  setThread(threadResult);
                  setThreadSearchQuery('');
                  setThreadSearchResults([]);
                }}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    setThread(threadResult);
                    setThreadSearchQuery('');
                    setThreadSearchResults([]);
                  }
                }}
              >
                <ForumIcon iconId={threadResult.iconId} className="icon" />
                {threadResult.title}
              </div>
            ))}
          </div>
        )}
        <FieldLabel>Start Date</FieldLabel>
        <FieldLabelSmall>Can&apos;t be in the past</FieldLabelSmall>
        <DateTimePicker
          value={startsAt}
          onChange={setStartsAt}
          minDate={nearestHour}
          calendarClassName="calendar"
          tileClassName="day"
          className="datepicker"
          renderSecondHand={false}
          clearIcon={null}
          calendarType="US"
        />
        <FieldLabel>End Date</FieldLabel>
        <FieldLabelSmall>
          Must be less than
          {` ${CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS} `}
          days after the start date
        </FieldLabelSmall>
        <DateTimePicker
          value={endsAt}
          onChange={setEndsAt}
          minDate={startsAt}
          maxDate={maxEndsAt}
          calendarClassName="calendar"
          tileClassName="day"
          className="datepicker"
          renderSecondHand={false}
          clearIcon={null}
          calendarType="US"
        />
        <FieldLabelSmall className="timezone">*Times are in your local timezone</FieldLabelSmall>
      </StyledModalContent>
    </Modal>
  );
};

export default CalendarEventFormModal;
