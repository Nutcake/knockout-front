import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { CalendarEvent } from 'knockout-schema';
import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import {
  createCalendarEvent,
  deleteCalendarEvent,
  getCalendarEvents,
  updateCalendarEvent,
} from '../../services/calendarEvents';
import { useAppSelector } from '../../state/hooks';
import { formatShortDate } from '../../utils/dateFormat';
import { pushSmartNotification } from '../../utils/notification';
import {
  ThemeBackgroundLighter,
  ThemeFontSizeHuge,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';
import Calendar from './components/Calendar';
import CalendarDayModal from './components/CalendarDayModal';
import CalendarEventFormModal from './components/CalendarEventFormModal';
import CalendarEventModal from './components/CalendarEventModal';
import {
  ADMIN,
  BASIC_USER,
  GOLD_USER,
  MODERATOR,
  MODERATOR_IN_TRAINING,
  PAID_GOLD_USER,
} from '../../utils/roleCodes';

dayjs.extend(relativeTime);

const StyledCalendarPage = styled.section`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h1 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  nav.subheader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .return-btn {
      display: block;
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};

      span {
        margin-left: calc(${ThemeHorizontalPadding} / 2);
      }
    }

    .back-and-title {
      display: flex;
      height: 30px;
      overflow: hidden;
      align-items: stretch;
      margin-right: ${ThemeHorizontalPadding};
      text-decoration: none;
    }

    .create-event-btn {
      cursor: pointer;
      display: block;
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};

      span {
        margin-left: calc(${ThemeHorizontalPadding} / 2);
      }
    }
  }
`;

const dateRange = (date: Date) => {
  const currentYear = date.getFullYear();
  const currentMonth = date.getMonth() + 1;

  const dayBuffer = new Date(currentYear, currentMonth - 1, 1).getDay();
  const startDate = new Date(currentYear, currentMonth - 1, 1);
  startDate.setDate(startDate.getDate() - dayBuffer);

  const endDate = new Date(startDate);
  endDate.setDate(startDate.getDate() + 35);

  return [startDate, endDate];
};

const canDestroyEvents = (roleCode: string) => {
  return [MODERATOR, MODERATOR_IN_TRAINING, ADMIN].includes(roleCode);
};

const canCreateEvents = (roleCode: string) => {
  return [BASIC_USER, GOLD_USER, PAID_GOLD_USER].includes(roleCode) || canDestroyEvents(roleCode);
};

const CalendarPage = () => {
  // param format: (YYYY-MM-DD)
  const currentDate = formatShortDate(new Date());

  const {
    params: { date = currentDate },
  } = useRouteMatch<{ date: string }>();

  // months are 0 indexed, so add 1 to month to get actual date
  const parsedDate = new Date(date);
  parsedDate.setDate(parsedDate.getDate() + 1);

  // the default date to send to our 'create event' form
  let formDefaultDate = new Date(parsedDate);
  // the user cannot create events in the past, so if the default date is in the past, set it to now
  if (formDefaultDate < new Date()) {
    formDefaultDate = new Date();
  }

  const [calendarEvents, setCalendarEvents] = useState<CalendarEvent[]>([]);
  const [eventFormModalOpen, setEventFormModalOpen] = useState(false);
  const [eventFormModalData, setEventFormModalData] = useState<CalendarEvent | null>(null);
  const [eventFormDefaultDate, setEventFormDefaultDate] = useState<Date>(formDefaultDate);
  const [showEventModalOpen, setShowEventModalOpen] = useState(false);
  const [showEventModalData, setShowEventModalData] = useState<CalendarEvent | null>(null);
  const [dayModalOpen, setDayModalOpen] = useState(false);
  const [dayModalEvents, setDayModalEvents] = useState<CalendarEvent[]>([]);
  const [dayModalDate, setDayModalDate] = useState<Date>(new Date());
  const currentUser = useAppSelector((state) => state.user);
  const currentUserId: number = currentUser?.id;
  const currentUserRoleCode: string = currentUser?.role?.code;

  useEffect(() => {
    const fetchCalendarEvents = async () => {
      // months are 0 indexed, so add 1 to month to get actual date
      const realDate = new Date(date);
      realDate.setDate(realDate.getDate() + 1);
      const range = dateRange(realDate);
      const events = await getCalendarEvents(range[0], range[1]);
      setCalendarEvents(events);
    };

    fetchCalendarEvents();
  }, [date]);

  const onEventCreate = async ({ title, description, threadId, startsAt, endsAt }) => {
    try {
      await createCalendarEvent({ title, description, threadId, startsAt, endsAt });
      pushSmartNotification({ message: 'Calendar Event created!' });
      const range = dateRange(parsedDate);
      const events = await getCalendarEvents(range[0], range[1]);
      setCalendarEvents(events);
      setEventFormModalOpen(false);
    } catch (err) {
      pushSmartNotification({ error: err.message });
    }
  };

  const onEventUpdate = async ({ title, description, threadId, startsAt, endsAt }) => {
    const id = eventFormModalData?.id;
    if (!id) {
      return;
    }

    try {
      await updateCalendarEvent({ id, title, description, threadId, startsAt, endsAt });
      pushSmartNotification({ message: 'Calendar Event updated!' });
      const range = dateRange(parsedDate);
      const events = await getCalendarEvents(range[0], range[1]);
      setCalendarEvents(events);
      setEventFormModalOpen(false);
    } catch (err) {
      pushSmartNotification({ error: err.message });
    }
  };

  const handleCreateEventClick = (defaultDate?: Date) => {
    if (defaultDate) {
      setEventFormDefaultDate(defaultDate);
    } else {
      setEventFormDefaultDate(formDefaultDate);
    }
    setEventFormModalData(null);
    setEventFormModalOpen(true);
  };

  const handleEventClick = (event: CalendarEvent) => {
    setShowEventModalData(event);
    setShowEventModalOpen(true);
  };

  const showEventByCurrentUser = showEventModalData?.createdBy?.id === currentUserId;
  const canCreate = canCreateEvents(currentUserRoleCode);
  const canDestroy = canDestroyEvents(currentUserRoleCode) || showEventByCurrentUser;
  const canEdit = showEventByCurrentUser && new Date(showEventModalData!.endsAt) > new Date();

  let handleEventEditClick: () => void = () => {};
  if (canEdit) {
    handleEventEditClick = () => {
      const { id, title, description, thread, startsAt, endsAt, createdBy, createdAt, updatedAt } =
        showEventModalData;

      setEventFormModalData({
        id,
        title,
        description,
        thread,
        startsAt,
        endsAt,
        createdBy,
        createdAt,
        updatedAt,
      });
      setShowEventModalOpen(false);
      setEventFormModalOpen(true);
    };
  }

  let handleEventDestroyClick: () => Promise<void> = async () => {};
  if (showEventByCurrentUser) {
    handleEventDestroyClick = async () => {
      const { id, title } = showEventModalData;
      // eslint-disable-next-line no-alert
      if (window.confirm(`Are you sure you want to remove "${title}?"`)) {
        try {
          await deleteCalendarEvent(id);
          pushSmartNotification({ message: 'Community Event removed!' });
          const newEvents = calendarEvents.filter((event) => event.id !== id);
          setCalendarEvents(newEvents);
          setShowEventModalOpen(false);
        } catch (err) {
          pushSmartNotification({ error: err.message });
        }
      }
    };
  }

  const handleDayClick = (day: Date, events: CalendarEvent[]) => {
    setDayModalDate(day);
    setDayModalEvents(events);
    setDayModalOpen(true);
  };

  return (
    <StyledCalendarPage>
      <Helmet>
        <title>Event Calendar</title>
      </Helmet>

      <h1>Community Event Calendar</h1>

      <nav className="subheader">
        <span className="back-and-title">
          <div className="left">
            <Link className="return-btn" to="/">
              <i className="fa-solid fa-angle-left" />
              <span>Home</span>
            </Link>
          </div>
        </span>

        {canCreate && (
          <div className="subheader-menu-item">
            <div
              className="create-event-btn"
              title="Create Calendar Event"
              tabIndex={0}
              role="button"
              onKeyDown={(e) => {
                if (e.key === 'Enter') handleCreateEventClick();
              }}
              onClick={() => handleCreateEventClick()}
            >
              <i className="fa-solid fa-plus">&nbsp;</i>
              <span>Create Event</span>
            </div>
          </div>
        )}
      </nav>

      <Calendar
        date={parsedDate}
        calendarEvents={calendarEvents}
        onCalendarEventClick={handleEventClick}
        onDayClick={handleDayClick}
        canCreate={canCreate}
        onCreateClick={handleCreateEventClick}
      />
      {canCreate && (
        <CalendarEventFormModal
          isOpen={eventFormModalOpen}
          onClose={() => {
            setEventFormModalData(null);
            setEventFormModalOpen(false);
          }}
          onSubmit={eventFormModalData ? onEventUpdate : onEventCreate}
          action={eventFormModalData ? 'update' : 'create'}
          defaultDate={eventFormDefaultDate}
          event={eventFormModalData}
        />
      )}
      {showEventModalData && (
        <CalendarEventModal
          isOpen={showEventModalOpen}
          close={() => {
            setShowEventModalOpen(false);
            setShowEventModalData(null);
          }}
          event={showEventModalData}
          canEdit={canEdit}
          onEdit={handleEventEditClick}
          canDestroy={canDestroy}
          onDestroy={handleEventDestroyClick}
        />
      )}
      {dayModalEvents.length > 0 && (
        <CalendarDayModal
          date={dayModalDate}
          isOpen={dayModalOpen}
          close={() => {
            setDayModalOpen(false);
            setDayModalEvents([]);
          }}
          events={dayModalEvents}
          onEventClick={(event) => {
            setDayModalOpen(false);
            setDayModalEvents([]);
            setShowEventModalData(event);
            setShowEventModalOpen(true);
          }}
        />
      )}
    </StyledCalendarPage>
  );
};

export default CalendarPage;
