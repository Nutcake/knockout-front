import { useEffect, useState } from 'react';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../../utils/postOptionsStorage';

export default (contentObject, content, match, getData) => {
  const [currentContentObject, setCurrentContentObject] = useState(contentObject);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

  const fetchData = async (page = 1) => {
    setCurrentPage(page);
    setLoading(true);
    setCurrentContentObject(await getData(match.params.id, page, nsfwFilterEnabled));
    setLoading(false);
  };

  useEffect(() => {
    if (currentPage !== 1 || !content) {
      fetchData(currentPage);
    } else {
      setLoading(false);
    }
  }, [currentPage, match.params.id]);

  return [currentContentObject, loading, fetchData];
};
