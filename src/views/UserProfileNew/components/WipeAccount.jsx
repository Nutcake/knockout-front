import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Modal from '../../../components/Modals/Modal';
import { TextField } from '../../../components/FormControls';
import { wipeAccount } from '../../../services/moderation';

const WipeAccountWarning = ({ user, modalOpen, setModalOpen }) => {
  const [username, setUsername] = useState('');
  const [reason, setReason] = useState('');

  return (
    <Modal
      title="Wipe account"
      iconUrl="static/icons/mushroom-cloud.png"
      submitText="Wipe account"
      submitFn={async () => {
        await wipeAccount(user.id, reason);
        setModalOpen(false);
        window.location.reload();
      }}
      cancelFn={() => setModalOpen(false)}
      isOpen={modalOpen}
      disableSubmit={user.username !== username || reason.length < 5}
    >
      <h2>Are you sure you want to wipe this user&apos;s account?</h2>
      <p>Wiping the account will:</p>
      <ul>
        <li>Permanently ban the user</li>
        <li>Remove the content of all of the user&apos;s posts, messages and profile comments</li>
        <li>Remove the user&apos;s avatar, background, and profile customizations</li>
        <li>Rename the user to &quot;DELETED_USER&quot;</li>
        <li>Remove all of the user&apos;s ratings</li>
      </ul>
      <p>
        If you still want to go through with this, enter a reason for wiping the account and the
        account&apos;s username below.
      </p>
      <TextField
        placeholder="Enter a reason for wiping the account"
        value={reason}
        onChange={(e) => setReason(e.target.value)}
      />
      <TextField
        placeholder="Enter the account's username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
    </Modal>
  );
};

WipeAccountWarning.propTypes = {
  modalOpen: PropTypes.bool.isRequired,
  setModalOpen: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired,
};

export default WipeAccountWarning;
