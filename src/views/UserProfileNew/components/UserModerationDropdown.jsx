import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import useDropdownMenu, {
  DropdownMenuOpened,
} from '../../../components/Header/components/DropdownMenu';
import {
  ThemeBackgroundLighter,
  ThemeFontFamily,
  ThemeFontSizeMedium,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import UserRoleRestricted from '../../../components/UserRoleRestricted';
import { MODERATOR_ROLES } from '../../../utils/roleCodes';

const UserModerationButton = styled.button`
  position: relative;
  z-index: 50;
  font-weight: normal;
  display: inline-block;
  appearance: none;
  color: white;
  background: ${ThemeBackgroundLighter};
  padding: calc(${ThemeHorizontalPadding} * 1.5) calc(${ThemeVerticalPadding} * 2);
  font-size: ${ThemeFontSizeMedium};
  font-family: ${ThemeFontFamily};
  text-align: center;
  text-decoration: none;
  white-space: nowrap;
  border: none;
  outline: none;
  margin-left: 10px;
  flex: 1;

  .moderation-icon {
    margin-right: 7px;
  }
`;

const UserModerationMenu = styled(DropdownMenuOpened)`
  min-width: 100px;
`;

const UserModerationDropdown = ({ children }) => {
  const menuRef = useRef();
  const buttonRef = useRef();
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);

  return (
    <UserRoleRestricted roleCodes={MODERATOR_ROLES}>
      <UserModerationButton
        info
        status
        ref={buttonRef}
        onClick={() => setOpen((value) => !value)}
        title="Moderation"
        type="button"
      >
        <i className="moderation-icon fa-solid fa-shield-alt" />
        <i className="fa-solid fa-angle-down" />
        {open && <UserModerationMenu ref={menuRef}>{children}</UserModerationMenu>}
      </UserModerationButton>
    </UserRoleRestricted>
  );
};

UserModerationDropdown.propTypes = {
  children: PropTypes.node.isRequired,
};
export default UserModerationDropdown;
