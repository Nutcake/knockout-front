import dayjs from 'dayjs';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import styled, { ThemeContext } from 'styled-components';
import { Helmet } from 'react-helmet';
import { rgba } from 'polished';
import config from '../../../config';
import UserRoleWrapper from '../../components/UserRoleWrapper';
import userRoles from '../../components/UserRoleWrapper/userRoles';
import ratingList from '../../utils/ratingList.json';
import { loadHideRatingsFromStorageBoolean } from '../../services/theme';
import {
  getPreviousUsernames,
  getUser,
  getUserBans,
  getUserProfile,
  getUserTopRatings,
  removeUserHeader,
} from '../../services/user';
import { updateBackgroundRequest } from '../../state/background';
import {
  ThemeBackgroundLighter,
  ThemeBannedUserColor,
  ThemeBodyBackgroundColor,
  ThemeBodyWidth,
  ThemeFontSizeHuge,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeModeratorColor,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';
import { Button, TextButton } from '../../components/Buttons';
import LinkedTabs from '../../components/Tabs/LinkedTabs';
import UserProfileOverview from './components/UserProfileOverview';
import UserProfilePosts from './components/UserProfilePosts';
import UserProfileThreads from './components/UserProfileThreads';
import UserProfileBans from './components/UserProfileBans';
import { MobileMediaQuery } from '../../components/SharedStyles';
import UserProfileEditor from './components/UserProfileEditor';
import UserModerationDropdown from './components/UserModerationDropdown';
import { DropdownMenuItem } from '../../components/Header/components/DropdownMenu';
import { removeUserImage, removeUserProfile, removeUserPronouns } from '../../services/moderation';
import BanModal from '../../components/BanModal';
import { isLoggedIn } from '../../utils/user';
import WipeAccountWarning from './components/WipeAccount';
import { isDeletedUser } from '../../utils/deletedUser';
import { MODERATOR_ROLES } from '../../utils/roleCodes';
import PreviousUsernamesDropdown from './components/PreviousUsernamesDropdown';

const StyledProfileWrapper = styled.div`
  .header {
    margin-top: calc(${ThemeVerticalPadding} * -1);
    width: 100%;
    overflow: hidden;
    max-height: calc(
      ${(props) =>
          props.theme.width === 'full' ? '100vw' : `min(${ThemeBodyWidth(props)}, 100vw)`} / 4.5
    );
    display: flex;
    align-items: center;
  }

  .header-image {
    width: 100%;
    display: block;
  }

  .profile-content {
    position: relative;
    z-index: 2;
    display: grid;
    grid-template-columns: 250px 1fr;
    column-gap: 75px;
    padding: 0 20px;
    padding-bottom: 20px;
    ${(props) =>
      props.theme.mode === 'light'
        ? 'background: rgba(0, 0, 0, 0) linear-gradient(rgb(255, 255, 255) 27%, rgba(255, 255, 255, 0.7) 50%, rgba(255, 255, 255, 0) 100%) repeat scroll 0% 0%;'
        : `background: rgba(0, 0, 0, 0) linear-gradient(${rgba(
            ThemeBodyBackgroundColor(props),
            1
          )} 27%, rgba(0, 0, 0, 0) 80%) repeat scroll 0% 0%;`}
    margin-top: -3px;
    ${MobileMediaQuery} {
      grid-template-columns: 1fr;
    }
  }

  .user-actions {
    display: flex;
    margin-top: 20px;
  }

  .profile-button {
    box-sizing: border-box;
    display: flex;
    align-items: center;
    flex-grow: 1;
    justify-content: center;
  }

  .ban-text {
    color: ${ThemeBannedUserColor};
  }

  .avatar {
    background: ${ThemeBackgroundLighter};
    width: 115px;
    height: 115px;
    margin-top: -60px;
    position: relative;

    ${MobileMediaQuery} {
      width: 70px;
      height: 70px;
      margin-top: -35px;
    }
  }

  .avatar-image {
    width: 100%;
    height: 100%;
    object-fit: contain;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
  }

  .user-pronouns {
    margin-top: ${ThemeVerticalPadding};
    font-size: ${ThemeFontSizeMedium};
    display: block;
    line-height: normal;
  }

  .user-pronouns {
    opacity: 60%;
  }

  .username {
    display: flex;
    align-items: center;
    margin-top: ${ThemeVerticalPadding};
    font-size: ${ThemeFontSizeHuge};
    font-weight: 600;
    line-height: normal;
  }

  .username-container {
    display: flex;
    align-items: center;
  }

  .online {
    overflow: visible;
  }

  .online-indicator {
    font-size: calc(${ThemeFontSizeSmall} / 1.5);
    padding-left: calc(${ThemeHorizontalPadding} / 2);
    color: ${ThemeModeratorColor};
    text-shadow: 0 0 1px black;
  }

  .join-date {
    margin-top: 5px;
    opacity: 60%;
  }

  .role {
    margin-top: 10px;
    display: flex;
    align-items: center;
    overflow: initial;
  }

  .role-icon-container {
    display: flex;
    margin-right: 7px;
    margin-left: 2px;
  }

  .role-icon {
    transform: rotate(45deg);
    font-size: 12px;
  }

  .user-summary {
    margin-top: 20px;
  }

  .summary-stat {
    font-weight: bold;
  }

  .summary-item {
    margin-right: 10px;
  }

  .user-bio {
    margin-top: 20px;
    line-height: 1.2em;
    overflow-wrap: break-word;
    white-space: pre-wrap;
  }

  .divider {
    margin: 20px 0;
    color: ${ThemeTextColor};
    opacity: ${(props) => (props.theme.mode === 'light' ? 0.2 : 0.5)};
  }

  .social-link {
    opacity: 60%;
    display: flex;
    align-items: center;
    margin-bottom: 12px;
    transition: 0.4s;
  }

  a.social-link:hover {
    opacity: 40%;
  }

  .social-link-icon {
    margin-right: 7px;
    font-size: 20px;
    width: 25px;
    text-align: center;
  }

  .ratings-header {
    display: flex;
    justify-content: space-between;
    margin-bottom: 10px;
  }

  .ratings-title {
    font-size: ${ThemeFontSizeLarge};
    font-weight: 600;
  }

  .ratings-link {
    font-size: ${ThemeFontSizeMedium};
    opacity: 60%;
    padding: 0;
  }

  .rating-item {
    display: flex;
    align-items: center;
    margin-bottom: 8px;
  }

  .rating-icon {
    width: 27px;
    margin-right: 7px;
  }

  .rating-count {
    margin-left: 7px;
    font-weight: bold;
  }
`;

const UserProfileNew = () => {
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const theme = useContext(ThemeContext);
  const currentUser = useSelector((state) => state.user);

  const [user, setUser] = useState({});
  const [posts, setPosts] = useState({});
  const [threads, setThreads] = useState([]);
  const [bans, setBans] = useState([]);
  const [topRatings, setTopRatings] = useState([]);
  const [previousUsernames, setPreviousUsernames] = useState([]);
  const [showAllRatings, setShowAllRatings] = useState(false);
  const [userProfile, setUserProfile] = useState({});
  const [profileEdit, setProfileEdit] = useState(false);
  const [banModalOpen, setBanModalOpen] = useState(false);
  const [wipeAccountModalOpen, setWipeAccountModalOpen] = useState(false);
  const [loaded, setLoaded] = useState(false);

  const header = useRef();

  const hasAvatar =
    user.avatarUrl && user.avatarUrl.length !== 0 && !user.avatarUrl.includes('none.webp');
  const url = `${config.cdnHost}/image/${user.avatarUrl}`;

  const fetchData = async () => {
    try {
      if (!match.params.id) {
        setLoaded(true);
        return;
      }

      const userRequest = getUser(match.params.id);
      const topRatingsRequest = getUserTopRatings(match.params.id);
      const userProfileRequest = getUserProfile(match.params.id);
      const bansRequest = getUserBans(match.params.id);
      setUser(await userRequest);
      if (!loadHideRatingsFromStorageBoolean()) {
        setTopRatings(await topRatingsRequest);
      }
      setUserProfile(await userProfileRequest);
      setBans(await bansRequest);
      if (MODERATOR_ROLES.includes(currentUser.role?.code)) {
        setPreviousUsernames(await getPreviousUsernames(match.params.id));
      }
    } catch (err) {
      console.error(err);
    }
    setLoaded(true);
  };

  useEffect(() => {
    if (userProfile.background?.url) {
      const backgroundUrl = `${config.cdnHost}/image/${userProfile.background.url}`;
      dispatch(updateBackgroundRequest(backgroundUrl, userProfile.background.type));
    } else {
      dispatch(updateBackgroundRequest(null));
    }
  }, [userProfile]);

  useEffect(() => {
    fetchData();
    return () => {
      dispatch(updateBackgroundRequest(null));
    };
  }, [match.params.id]);

  useEffect(() => {
    const parallax = () => {
      header.current.style.transform = `translateY(${window.scrollY * 0.5}px)`;
    };
    window.addEventListener('scroll', parallax, { capture: true, passive: true });

    return () => {
      window.removeEventListener('scroll', parallax, { capture: true, passive: true });
    };
  }, []);

  if (loaded && user.id === undefined) {
    return <Redirect to="/" />;
  }

  let ratings = topRatings.sort((a, b) => (a.count > b.count ? -1 : 1));
  if (!showAllRatings) ratings = ratings.slice(0, 3);

  const defaultHeader =
    theme.mode === 'light' ? 'static/profile_header.png' : 'static/profile_header_dark.png';
  const headerImage = userProfile.header
    ? `${config.cdnHost}/image/${userProfile.header}`
    : defaultHeader;

  const tabs = [{ name: 'Overview', path: match.url, exact: true }];

  if (user.posts) tabs.push({ name: 'Posts', path: `${match.url}/posts` });
  if (user.threads) tabs.push({ name: 'Threads', path: `${match.url}/threads` });
  if (bans.length) tabs.push({ name: 'Mutes', path: `${match.url}/bans` });

  const canMessageUser =
    currentUser.loggedIn &&
    currentUser.id !== user.id &&
    (!user.disableIncomingMessages || MODERATOR_ROLES.includes(currentUser.role?.code));

  return (
    <StyledProfileWrapper>
      <Helmet>
        <title>{user.username ? `${user.username}'s Profile - Knockout!` : 'Knockout!'}</title>
      </Helmet>
      <UserProfileEditor
        closeFn={() => setProfileEdit(false)}
        isOpen={profileEdit}
        profile={userProfile}
        headerImage={headerImage}
        callback={(
          profile,
          avatarUrl,
          pronouns,
          useBioForTitle,
          showOnlineStatus,
          disableIncomingMessages
        ) => {
          setUserProfile(profile);
          setUser({
            ...user,
            avatarUrl,
            pronouns,
            useBioForTitle,
            showOnlineStatus,
            disableIncomingMessages,
            online: showOnlineStatus,
          });
        }}
        userPronouns={user.pronouns}
        userUseBioForTitle={user.useBioForTitle}
        userShowOnlineStatus={user.showOnlineStatus}
        userDisableIncomingMessages={user.disableIncomingMessages}
      />
      <div className="header" ref={header}>
        <img className="header-image" alt="Profile header" src={headerImage} />
      </div>
      <div className="profile-content">
        <div className="profile-user">
          {hasAvatar && (
            <div className="avatar">
              <img className="avatar-image" src={url} alt={`${user.username}'s Avatar`} />
            </div>
          )}
          <div className="username-container">
            <UserRoleWrapper
              className={`username${user.online && user.showOnlineStatus ? ' online' : ''}`}
              user={user}
            >
              {user.username}
              {user.online && <i className="fas fa-circle online-indicator" />}
            </UserRoleWrapper>
            {previousUsernames.length > 0 && (
              <PreviousUsernamesDropdown previousUsernames={previousUsernames} />
            )}
          </div>

          <div className="join-date">
            {`Member since ${dayjs(user.createdAt).format('MMMM YYYY')}`}
          </div>
          {user.pronouns && (
            <div className="user-pronouns" title="Pronouns">
              {user.pronouns}
            </div>
          )}
          <UserRoleWrapper className="role" user={user}>
            <div className="role-icon-container">
              <i className="fa-solid fa-square role-icon" />
            </div>
            {userRoles[user.role?.code]?.name}
          </UserRoleWrapper>
          <div className="user-bio">{userProfile.bio}</div>
          <div className="user-summary">
            {user.posts > 0 && (
              <Link to={`${match.url}/posts`}>
                <span className="summary-stat">{user.posts}</span>
                &nbsp;
                <span className="summary-item">{user.posts === 1 ? 'post' : 'posts'}</span>
              </Link>
            )}
            {user.threads > 0 && (
              <Link to={`${match.url}/threads`}>
                <span className="summary-stat">{user.threads}</span>
                &nbsp;
                <span className="summary-item">{user.threads === 1 ? 'thread' : 'threads'}</span>
              </Link>
            )}
          </div>
          {isLoggedIn() && (
            <div className="user-actions">
              {currentUser.id === user.id && (
                <Button className="profile-button" onClick={() => setProfileEdit(true)}>
                  Edit profile
                </Button>
              )}
              {canMessageUser && (
                <Button className="profile-button" as={Link} to={`/messages/new/${user.id}`}>
                  Message
                </Button>
              )}
              <UserModerationDropdown>
                <DropdownMenuItem
                  role="button"
                  tabIndex="0"
                  onClick={async () => {
                    await removeUserImage({ userId: user.id, avatar: true });
                    setUser({ ...user, avatarUrl: undefined });
                  }}
                >
                  Remove avatar
                </DropdownMenuItem>
                <DropdownMenuItem
                  role="button"
                  tabIndex="0"
                  onClick={async () => {
                    await removeUserHeader(user.id);
                    setUserProfile({ ...userProfile, header: undefined });
                  }}
                >
                  Remove header
                </DropdownMenuItem>
                <DropdownMenuItem
                  role="button"
                  tabIndex="0"
                  onClick={() => removeUserImage({ userId: user.id, background: true })}
                >
                  Remove post background
                </DropdownMenuItem>
                <DropdownMenuItem
                  role="button"
                  tabIndex="0"
                  onClick={async () => {
                    await removeUserPronouns(user.id);
                    setUser({ ...user, pronouns: '' });
                  }}
                >
                  Remove pronouns
                </DropdownMenuItem>
                <DropdownMenuItem
                  role="button"
                  tabIndex="0"
                  onClick={async () => {
                    await removeUserProfile(user.id);
                    setUserProfile({});
                  }}
                >
                  Remove profile customizations
                </DropdownMenuItem>
                {!isDeletedUser(user.username) && (
                  <DropdownMenuItem
                    className="ban-text"
                    role="button"
                    tabIndex="0"
                    onClick={() => setBanModalOpen(true)}
                  >
                    Ban user
                  </DropdownMenuItem>
                )}
                {!isDeletedUser(user.username) && (
                  <DropdownMenuItem
                    className="ban-text"
                    role="button"
                    tabIndex="0"
                    onClick={() => setWipeAccountModalOpen(true)}
                  >
                    Wipe account
                  </DropdownMenuItem>
                )}
              </UserModerationDropdown>
              <BanModal
                userId={user.id}
                isOpen={banModalOpen}
                submitFn={() => setBanModalOpen(false)}
                cancelFn={() => setBanModalOpen(false)}
              />
              <WipeAccountWarning
                user={user}
                modalOpen={wipeAccountModalOpen}
                setModalOpen={setWipeAccountModalOpen}
              />
            </div>
          )}
          {userProfile.social && Object.keys(userProfile.social).length > 0 && (
            <hr className="divider" />
          )}
          {userProfile.social?.website && (
            <a className="social-link" href={userProfile.social.website} target="_blank">
              <i className="fa-solid fa-link social-link-icon" />
              {userProfile.social.website}
            </a>
          )}
          {userProfile.social?.steam?.name && (
            <a className="social-link" href={userProfile.social.steam.url} target="_blank">
              <i className="fa-brands fa-steam social-link-icon" />
              {userProfile.social.steam.name}
            </a>
          )}
          {userProfile.social?.twitter && (
            <a
              className="social-link"
              href={`https://twitter.com/${userProfile.social.twitter}`}
              target="_blank"
            >
              <i className="fa-brands fa-twitter social-link-icon" />
              {userProfile.social.twitter}
            </a>
          )}
          {userProfile.social?.fediverse && (
            <div className="social-link">
              <i className="fa-brands fa-mastodon social-link-icon" />
              {userProfile.social.fediverse}
            </div>
          )}
          {userProfile.social?.twitch && (
            <a
              className="social-link"
              href={`https://twitch.tv/${userProfile.social.twitch}`}
              target="_blank"
            >
              <i className="fa-brands fa-twitch social-link-icon" />
              {userProfile.social.twitch}
            </a>
          )}
          {userProfile.social?.discord && (
            <div className="social-link">
              <i className="fa-brands fa-discord social-link-icon" />
              {userProfile.social.discord}
            </div>
          )}
          {userProfile.social?.github && (
            <a
              className="social-link"
              href={`https://github.com/${userProfile.social.github}`}
              target="_blank"
            >
              <i className="fa-brands fa-github social-link-icon" />
              {userProfile.social.github}
            </a>
          )}
          {userProfile.social?.gitlab && (
            <a
              className="social-link"
              href={`https://gitlab.com/${userProfile.social.gitlab}`}
              target="_blank"
            >
              <i className="fa-brands fa-gitlab social-link-icon" />
              {userProfile.social.gitlab}
            </a>
          )}
          {userProfile.social?.tumblr && (
            <a
              className="social-link"
              href={`https://${userProfile.social.tumblr}.tumblr.com`}
              target="_blank"
            >
              <i className="fa-brands fa-tumblr-square social-link-icon" />
              {userProfile.social.tumblr}
            </a>
          )}
          {topRatings.length > 0 && (
            <>
              <hr className="divider" />
              <div className="ratings-header">
                <span className="ratings-title">Ratings</span>
                <TextButton
                  onClick={() => setShowAllRatings((value) => !value)}
                  className="ratings-link"
                >
                  {showAllRatings ? 'See less' : 'See all'}
                </TextButton>
              </div>
              <div className="ratings">
                {ratings.map((rating) => (
                  <div key={rating.name} className="rating-item">
                    <img
                      className="rating-icon"
                      src={ratingList[rating.name].url}
                      alt={ratingList[rating.name].name}
                    />
                    {ratingList[rating.name].name}
                    <span className="rating-count">{rating.count}</span>
                  </div>
                ))}
              </div>
            </>
          )}
        </div>
        <div className="profile-activity">
          <LinkedTabs tabs={tabs} />
          <Switch>
            <Route exact path={match.url}>
              <UserProfileOverview
                user={user}
                posts={posts}
                setPosts={setPosts}
                threads={threads}
                setThreads={setThreads}
                showRatings={topRatings.length > 0}
                match={match}
                commentsDisabled={userProfile.disableComments}
              />
            </Route>
            <Route path={`${match.url}/posts`}>
              <UserProfilePosts
                posts={posts}
                showRatings={topRatings.length > 0}
                user={user}
                match={match}
              />
            </Route>
            <Route path={`${match.url}/threads`}>
              <UserProfileThreads threads={threads} match={match} />
            </Route>
            <Route path={`${match.url}/bans`}>
              <UserProfileBans user={user} match={match} />
            </Route>
          </Switch>
        </div>
      </div>
    </StyledProfileWrapper>
  );
};

export default UserProfileNew;
