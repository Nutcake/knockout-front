import React, { useEffect, useState } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { KnockoutCommit } from 'knockout-schema';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { getChangelog } from '../../services/changelogs';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeFontSizeHuge,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import Pagination from '../../components/Pagination';
import Commit from './components/Commit';

const TOTAL_COMMITS_TO_DISPLAY = 2500;

const StyledChangelogPage = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  nav.subheader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .return-btn {
      display: block;
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};

      span {
        margin-left: calc(${ThemeHorizontalPadding} / 2);
      }
    }

    .back-and-title {
      display: flex;
      height: 30px;
      overflow: hidden;
      align-items: stretch;
      margin-right: ${ThemeHorizontalPadding};
      text-decoration: none;
    }
  }

  .subheader.pagination {
    justify-content: flex-end;
  }

  .loading,
  .blank-slate {
    margin-top: ${ThemeVerticalPadding};
  }
`;

const ChangelogPage = () => {
  const [commits, setCommits] = useState<Array<KnockoutCommit>>([]);
  const [loading, setLoading] = useState(true);
  const match = useRouteMatch<{ page: string }>();

  const page = match.params.page || '1';

  useEffect(() => {
    const fetchCommits = async () => {
      setLoading(true);
      const result = await getChangelog(Number(page));
      setCommits(result);
      setLoading(false);
    };
    fetchCommits();
  }, [page]);

  let changelogContent;

  if (loading) {
    changelogContent = <div className="loading">Loading changelog...</div>;
  } else if (commits.length === 0) {
    changelogContent = <div className="blank-slate">There are no more commits to display.</div>;
  } else {
    changelogContent = commits.map((commit) => {
      return (
        <Commit
          key={commit.id}
          id={commit.id}
          authorName={commit.authorName}
          userId={commit.userId}
          title={commit.title}
          projectName={commit.projectName}
          date={commit.date}
          url={commit.url}
        />
      );
    });
  }

  return (
    <StyledChangelogPage>
      <Helmet>
        <title>Changelog - Knockout!</title>
      </Helmet>
      <h2>Changelog</h2>

      <nav className="subheader">
        <span className="back-and-title">
          <div className="left">
            <Link className="return-btn" to="/">
              <i className="fas fa-angle-left" />
              <span>Home</span>
            </Link>
          </div>
        </span>
        <Pagination
          showNext
          pagePath="/changelog/"
          totalPosts={TOTAL_COMMITS_TO_DISPLAY}
          currentPage={page}
          pageSize={30}
        />
      </nav>
      {changelogContent}
      <nav className="subheader pagination">
        <Pagination
          showNext
          pagePath="/changelog/"
          totalPosts={TOTAL_COMMITS_TO_DISPLAY}
          currentPage={page}
          pageSize={30}
        />
      </nav>
    </StyledChangelogPage>
  );
};

export default ChangelogPage;
