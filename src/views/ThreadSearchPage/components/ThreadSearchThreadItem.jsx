import React from 'react';
import PropTypes from 'prop-types';
import ThreadItem from '../../../components/ThreadItem';

const ThreadSearchThreadItem = ({
  id,
  createdAt,
  deleted,
  iconId,
  lastPost,
  locked,
  pinned,
  postCount,
  title,
  user,
  tags,
  backgroundUrl,
  backgroundType,
  viewers,
  subforumName,
}) => {
  return (
    <ThreadItem
      id={id}
      createdAt={createdAt}
      deleted={deleted}
      iconId={iconId}
      lastPost={lastPost}
      locked={locked}
      pinned={pinned}
      postCount={postCount}
      title={title}
      user={user}
      tags={tags}
      backgroundUrl={backgroundUrl}
      backgroundType={backgroundType}
      viewers={viewers}
      subforumName={subforumName}
    />
  );
};

ThreadSearchThreadItem.propTypes = {
  id: PropTypes.number.isRequired,
  createdAt: PropTypes.string.isRequired,
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string.isRequired,
  }).isRequired,
  deleted: PropTypes.bool,
  iconId: PropTypes.number.isRequired,
  lastPost: PropTypes.shape({
    id: PropTypes.number.isRequired,
    createdAt: PropTypes.string.isRequired,
    user: PropTypes.shape({
      avatarUrl: PropTypes.string,
      username: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  locked: PropTypes.bool,
  pinned: PropTypes.bool,
  postCount: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  tags: PropTypes.arrayOf(PropTypes.object),
  backgroundUrl: PropTypes.string,
  backgroundType: PropTypes.string,
  viewers: PropTypes.shape({
    memberCount: PropTypes.number.isRequired,
    guestCount: PropTypes.number.isRequired,
  }),
  subforumName: PropTypes.string,
};

ThreadSearchThreadItem.defaultProps = {
  deleted: false,
  locked: false,
  pinned: false,
  tags: [{}],
  backgroundUrl: '',
  backgroundType: '',
  viewers: undefined,
  subforumName: '',
};

export default ThreadSearchThreadItem;
