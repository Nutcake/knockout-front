import React, { useEffect } from 'react';
import { authPost } from '../../services/common';
import { removeUserFromStorage } from '../../services/user';

const Logout = () => {
  useEffect(() => {
    authPost({ url: '/auth/logout' }).then(() => {
      removeUserFromStorage();
      window.location.href = '/';
    });
  }, []);

  return <></>;
};

export default Logout;
