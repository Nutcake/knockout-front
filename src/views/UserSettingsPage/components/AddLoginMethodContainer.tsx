import React from 'react';
import { Panel, PanelTitle } from '../../../components/Panel';
import LoginButton from '../../../components/LoginButton';

const AddLoginMethodContainer = () => {
  return (
    <Panel>
      <PanelTitle title="Add Login Method">Add Login Method</PanelTitle>
      <LoginButton color="white" background="#d14836" route="/auth/google/login">
        <i className="fa-brands fa-google" />
        <span>Google</span>
      </LoginButton>
      <LoginButton color="white" background="#24292e" route="/auth/github/login">
        <i className="fa-brands fa-github" />
        <span>GitHub</span>
      </LoginButton>
      <LoginButton color="white" background="#171a21" route="/auth/steam/login">
        <i className="fa-brands fa-steam" />
        <span>Steam</span>
      </LoginButton>
    </Panel>
  );
};

export default AddLoginMethodContainer;
