import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import ForumIcon from '../../../components/ForumIcon';

import Modal from '../../../components/Modals/Modal';
import { getHiddenThreads, unhideThread } from '../../../services/hiddenThreads';
import { pushSmartNotification } from '../../../utils/notification';
import {
  ThemeFontSizeSmall,
  ThemeBackgroundLighter,
  ThemeFontSizeHuge,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeHighlightStronger,
  ThemeTextColor,
} from '../../../utils/ThemeNew';

const StyledHiddenThreads = styled.div`
  .info {
    margin: 0;
    padding-bottom: ${ThemeVerticalPadding};
    font-size: ${ThemeFontSizeSmall};
  }

  .thread {
    display: flex;
    overflow: hidden;
    font-size: ${ThemeFontSizeSmall};
    align-items: center;
    height: 30px;
    box-sizing: border-box;
    padding: 0;

    .show-btn {
      cursor: pointer;
      background: none;
      border: none;

      .show-icon {
        color: ${ThemeTextColor};
      }

      &:hover,
      &:focus {
        .show-icon {
          color: ${ThemeHighlightStronger};
        }
      }
    }

    &:hover {
      background: ${ThemeBackgroundLighter};
    }

    .icon {
      width: ${ThemeFontSizeHuge};
      margin-left: calc(${ThemeHorizontalPadding} / 2);
      margin-right: calc(${ThemeHorizontalPadding} / 2);
    }
  }
`;

interface HiddenThreadsProps {
  modalOpen: boolean;
  setModalOpen: Function;
}

const HiddenThreads = ({ modalOpen, setModalOpen }: HiddenThreadsProps) => {
  const [hiddenThreads, setHiddenThreads] = useState([]);

  useEffect(() => {
    const fetchHiddenThreads = async () => {
      const threads = await getHiddenThreads();
      setHiddenThreads(threads);
    };
    if (modalOpen) {
      fetchHiddenThreads();
    }
  }, [modalOpen]);

  const showThread = async (id: number) => {
    try {
      await unhideThread(id);
      const newThreads = hiddenThreads.filter((thread) => thread.id !== id);
      setHiddenThreads(newThreads);
      pushSmartNotification({ message: 'Thread unhidden.' });
    } catch (err) {
      pushSmartNotification({ error: 'Could not unhide thread.' });
    }
  };

  return (
    <Modal
      title="Hidden Threads"
      cancelFn={() => setModalOpen(false)}
      isOpen={modalOpen}
      cancelText="Close"
    >
      <StyledHiddenThreads>
        {hiddenThreads.length === 0 ? (
          <p>You have not hidden any threads.</p>
        ) : (
          hiddenThreads.map((thread) => {
            return (
              <div key={thread.id} className="thread">
                <ForumIcon iconId={thread.iconId} className="icon" />
                {thread.title}
                <button
                  type="button"
                  className="show-btn"
                  title="Show thread"
                  onClick={() => showThread(thread.id)}
                >
                  <i className="fa-solid fa-eye show-icon" />
                </button>
              </div>
            );
          })
        )}
      </StyledHiddenThreads>
    </Modal>
  );
};

export default HiddenThreads;
