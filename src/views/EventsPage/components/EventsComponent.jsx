import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { CSSTransition, TransitionGroup } from 'react-transition-group';
import {
  ThemeHorizontalPadding,
  ThemeBackgroundDarker,
  ThemeFontSizeHuge,
} from '../../../utils/ThemeNew';
import Event from './Event';

const EventsComponent = ({ events, eventsLoaded, initialLoad }) => {
  return (
    <StyledEventsPage initialLoad={initialLoad}>
      <h2>Ticker</h2>
      <ul className="events">
        <TransitionGroup>
          {eventsLoaded &&
            events.length > 0 &&
            events.map((event) => (
              <CSSTransition timeout={1000} classNames="event" key={event.id} in>
                <Event event={event} />
              </CSSTransition>
            ))}
        </TransitionGroup>
        {eventsLoaded && !events.length && (
          <li className="empty-list">
            <p>Looks like there are no events </p>
          </li>
        )}
      </ul>
    </StyledEventsPage>
  );
};

EventsComponent.propTypes = {
  events: PropTypes.arrayOf(PropTypes.object).isRequired,
  eventsLoaded: PropTypes.bool.isRequired,
  initialLoad: PropTypes.bool.isRequired,
};

export const StyledEventsPage = styled.div`
  padding: 0 ${ThemeHorizontalPadding};
  .events {
    list-style: none;
    padding: 10px;
    background: ${ThemeBackgroundDarker};
  }

  .event-enter {
    max-height: 0;
    margin-bottom: 0;
    opacity: 0.01;
  }

  .event-enter.event-enter-active,
  .event-enter-done {
    max-height: 400px;
    margin-bottom: 8px;
    opacity: 1;
    transition: ${(props) =>
        !props.initialLoad && 'max-height 1000ms ease-in-out, margin-bottom 1000ms ease-in-out,'}
      opacity 400ms ${(props) => !props.initialLoad && '500ms'};
  }

  .empty-list {
    font-size: 2.8rem;
    padding: 30px 0;
    text-align: center;
    text-transform: capitalize;
    opacity: 0.3;
  }
  h2 {
    font-size: ${ThemeFontSizeHuge};
  }
  @media (max-width: 700px) {
    .empty-list {
      font-size: 1.5rem;
    }
  }
`;

export default EventsComponent;
