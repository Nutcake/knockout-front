/* eslint-disable react/display-name */
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useSpinDelay } from 'spin-delay';
import ForumIcon from '../../../components/ForumIcon';
import Tooltip from '../../../components/Tooltip';
import {
  loadLatestThreadModeFromStorage,
  setLatestThreadModeToStorage,
  loadThreadAdsFromStorageBoolean,
  loadFilteredSubforumsFromStorage,
  setFilteredSubforumsToStorage,
} from '../../../services/theme';
import filterNsfwThreads from '../../../utils/filterNsfwThreads';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../../utils/postOptionsStorage';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { buttonHover } from '../../../components/SharedStyles';
import getRandomThreadAd from '../../../services/threadAds';
import Placeholder from '../../../components/Placeholder';
import { getLatestThreads, getPopularThreads } from '../../../services/threads';
import { formattedUsername } from '../../../utils/user';
import SubforumFilters from './SubforumFilters';

dayjs.extend(relativeTime);

const LatestAndPopularWrapper = styled.section`
  .header {
    background: ${ThemeBackgroundLighter};
    min-height: 60px;
    display: grid;
    grid-template-columns: 1fr 60px;
    align-content: center;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    box-sizing: border-box;
    font-weight: bold;

    > .heading-wrapper {
      align-items: flex-start;
      display: flex;
      flex-direction: column;
      justify-content: center;

      > .heading {
        margin-bottom: calc(${ThemeVerticalPadding} / 2);
      }

      > .heading-description {
        margin: 0;
        font-size: ${ThemeFontSizeSmall};
        opacity: ${(props) => (props.theme.mode === 'light' ? '0.7' : '0.5')};
      }
    }

    p {
      margin: 0;
      margin-top: ${ThemeVerticalPadding};
      font-weight: initial;
      font-size: ${ThemeFontSizeSmall};
      opacity: 0.5;
    }

    button {
      border: none;
      background: ${ThemeBackgroundDarker};
      color: ${ThemeTextColor};
      width: 30px;
      height: 40px;
      margin-left: 5px;

      ${buttonHover}

      &.active:not(:hover) {
        background: #c71212;
      }
    }
  }

  .thread-container {
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    box-sizing: border-box;
    overflow: hidden auto;
    scrollbar-width: thin;
    scrollbar-color: ${ThemeBackgroundLighter} ${ThemeBackgroundDarker};
    min-height: 400px;
    max-height: 640px;
    &::-webkit-scrollbar-track {
      background-color: ${ThemeBackgroundDarker};
    }

    &::-webkit-scrollbar {
      width: 6px;
      height: 10px;
      background-color: ${ThemeBackgroundLighter};
    }

    &::-webkit-scrollbar-thumb {
      opacity: 0.5;
      background-color: ${ThemeBackgroundLighter};
    }

    .thread-item {
      width: 100%;
      display: grid;
      grid-template-columns: 60px 1fr;
      align-items: center;
      font-size: ${ThemeFontSizeMedium};
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

      .title {
        /* Hacky way to clamp text to a given line count until line-clamp is implemented for realsies */
        /* Graciously acquired from: https://css-tricks.com/line-clampin/ */
        display: -webkit-box; /* stylelint-disable-line value-no-vendor-prefix */
        overflow: hidden;
        -webkit-box-orient: vertical; /* stylelint-disable-line property-no-vendor-prefix */
        -webkit-line-clamp: 2; /* stylelint-disable-line property-no-vendor-prefix */

        line-height: 1.3em;
        margin-bottom: calc(${ThemeVerticalPadding} / 2);
      }

      .info {
        margin-right: ${ThemeHorizontalPadding};
      }

      .description {
        font-size: ${ThemeFontSizeSmall};
        opacity: ${(props) => (props.theme.mode === 'light' ? '0.7' : '0.5')};
      }

      .subforum {
        font-size: ${ThemeFontSizeSmall};
        font-style: italic;
        margin-bottom: 4px;
      }

      img {
        width: 40px;
        height: auto;
      }

      p {
        margin: 0;
      }

      &:hover {
        background-color: ${ThemeBackgroundLighter};
        cursor: pointer;

        .title {
          text-decoration: underline;
        }
      }
    }

    & .no-threads-found-container {
      margin-top: ${ThemeVerticalPadding};
      display: flex;
      justify-content: center;

      & span {
        font-size: ${ThemeFontSizeSmall};
      }
    }
  }

  .thread-ads {
    margin-top: ${ThemeVerticalPadding};
    background: ${ThemeBackgroundDarker};

    h6 {
      font-size: ${ThemeFontSizeSmall};
      padding: calc(${ThemeVerticalPadding} / 2) 0;
      padding-right: calc(${ThemeHorizontalPadding} / 2);
      text-align: right;
      opacity: 0.3;
    }
  }

  .buttons {
    display: flex;

    & i.fa-solid {
      width: 13px;
    }
  }
`;

const initialView = loadLatestThreadModeFromStorage() ? 1 : 0;
const initialFilteredSubforums = loadFilteredSubforumsFromStorage();

const buildThreadInfo = (currentView, thread) => {
  if (currentView === 0) {
    return `${dayjs(thread.createdAt).fromNow()}`;
  }

  return thread.viewers?.memberCount !== undefined
    ? `${thread.viewers.memberCount} reading now`
    : `${thread.recentPostCount} recent replies`;
};

const getSubforumNameForThread = (thread, subforums) => {
  if (!Array.isArray(subforums) || typeof thread.subforumId === 'undefined') {
    return 'N/A';
  }

  const threadSubforum = subforums.find((subforum) => subforum.id === thread.subforumId);

  if (typeof threadSubforum === 'undefined') {
    return 'N/A';
  }

  return threadSubforum.name;
};

const getThreadAdComponent =
  ({ description, query, imageUrl }) =>
  () =>
    (
      <div className="thread-ads">
        <Link to={`/threadsearch/${query}`}>
          <img title={description} alt={description} src={imageUrl} />
          <h6>Random Thread</h6>
        </Link>
      </div>
    );

const LatestAndPopular = ({ subforumList }) => {
  const [currentView, setCurrentView] = useState(initialView); // 0: latest; 1: popular
  const [lastLoadedView, setLastLoadedView] = useState(initialView); // same as above
  const [threadList, setThreadList] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [initialLoading, setInitialLoading] = useState(true);
  const [threadAd, setThreadAd] = useState(undefined);
  const [showFilters, setShowFilters] = useState(false);
  const [tempFilteredSubforums, setTempFilteredSubforums] = useState(initialFilteredSubforums);
  const [filteredSubforums, setFilteredSubforums] = useState(initialFilteredSubforums); // [subforum_id] (if it exists in the array it should be filtered out)

  const showLoading = useSpinDelay(!loaded);

  const displayThreadAds = loadThreadAdsFromStorageBoolean();

  const placeholders = Array(10)
    .fill(1)
    .map((item, index) => (
      // eslint-disable-next-line react/no-array-index-key
      <div className="thread-item" key={`p-${index}`}>
        <Placeholder width={48} height={48} />
        <div className="info">
          <Placeholder width={100} />
          <Placeholder width={150} />
        </div>
      </div>
    ));

  const getThreads = async () => {
    setLoaded(false);
    const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();
    const threads =
      currentView === 0
        ? await getLatestThreads(filteredSubforums)
        : await getPopularThreads(filteredSubforums);

    let filteredThreads;
    if (nsfwFilterEnabled) {
      filteredThreads = filterNsfwThreads(threads);
    } else {
      filteredThreads = threads;
    }

    setThreadList(filteredThreads);
    setLoaded(true);
    setLastLoadedView(currentView);

    if (initialLoading) {
      setInitialLoading(false);
    }
  };

  useEffect(() => {
    if (!showFilters) {
      setFilteredSubforums(tempFilteredSubforums);
      setFilteredSubforumsToStorage(tempFilteredSubforums);
    }
  }, [showFilters]);

  useEffect(() => {
    getThreads();
  }, [currentView, filteredSubforums]);

  useEffect(() => {
    const getRandomAd = async () => {
      if (!displayThreadAds) {
        return;
      }

      const ad = await getRandomThreadAd();

      setThreadAd(ad);
    };

    getRandomAd();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  let MemoizedThreadAd;

  if (displayThreadAds && threadAd !== undefined) {
    MemoizedThreadAd = React.memo(getThreadAdComponent(threadAd));
  }

  const isOnLatestThreads = lastLoadedView === 0;
  const headerTitle = isOnLatestThreads ? 'Latest Threads' : 'Popular Threads';
  const buttonTitle = isOnLatestThreads ? 'View Popular Threads' : 'View Latest Threads';
  const headerDescription = isOnLatestThreads
    ? 'The most recently created threads'
    : 'Most active threads as of late';
  const buttonClass = isOnLatestThreads ? 'fa-solid fa-clock' : 'fa-solid fa-fire';

  const switchMode = async () => {
    setCurrentView(isOnLatestThreads ? 1 : 0);
    setLatestThreadModeToStorage(isOnLatestThreads ? 1 : 0);
  };

  const switchShowFilters = () => {
    setShowFilters(!showFilters);
  };

  return (
    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    <LatestAndPopularWrapper>
      <div className="header">
        <span className="heading-wrapper">
          <span className="heading">{headerTitle}</span>
          <p className="heading-description">{headerDescription}</p>
        </span>
        <div className="buttons">
          <button
            disabled={!loaded}
            className={`icon-button${showFilters ? ' active' : ''}`}
            type="button"
            onClick={switchShowFilters}
            title="Filter Subforums"
          >
            <i
              className={!showLoading ? 'fa-solid fa-filter' : 'fa-solid fa-circle-notch fa-spin'}
            />
          </button>
          <button
            disabled={!loaded}
            className="icon-button"
            type="button"
            onClick={switchMode}
            title={buttonTitle}
          >
            <i className={!showLoading ? buttonClass : 'fa-solid fa-circle-notch fa-spin'} />
          </button>
        </div>
      </div>
      {showFilters ? (
        <SubforumFilters
          subforumList={subforumList}
          loadingSubforums={initialLoading}
          tempFilteredSubforums={tempFilteredSubforums}
          setTempFilteredSubforums={setTempFilteredSubforums}
        />
      ) : null}
      <div className="thread-container">
        {initialLoading
          ? placeholders
          : threadList
              .filter((thread) => thread.id !== undefined)
              .map((thread, index) => (
                <Tooltip
                  key={thread.id}
                  top={index !== 0}
                  text={`Created by ${formattedUsername(thread.user?.username)}, last reply ${dayjs(
                    thread.updatedAt
                  ).fromNow()}`}
                  widthLimited
                >
                  <Link to={`/thread/${thread.id}`} className="thread-item" title={thread.title}>
                    <ForumIcon iconId={thread.iconId} />
                    <div className="info">
                      <p className="title">{thread.title}</p>
                      <p className="description">
                        {buildThreadInfo(lastLoadedView, thread)}
                        <span> in </span>
                        {getSubforumNameForThread(thread, subforumList)}
                      </p>
                    </div>
                  </Link>
                </Tooltip>
              ))}
        {!threadList.length && !initialLoading && (
          <div className="no-threads-found-container">
            <span>No threads found. Check your filters?</span>
          </div>
        )}
      </div>

      {MemoizedThreadAd && <MemoizedThreadAd />}
    </LatestAndPopularWrapper>
  );
};

LatestAndPopular.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  subforumList: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default LatestAndPopular;
