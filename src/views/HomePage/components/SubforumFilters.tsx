import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Checkbox, CheckboxStates } from '../../../components/Checkbox';
import Placeholder from '../../../components/Placeholder';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';

const SubforumFilterWrapper = styled.div`
  width: 100%;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  padding-top: 0;
  background-color: ${ThemeBackgroundLighter};
  box-sizing: border-box;

  & > .container {
    background-color: ${ThemeBackgroundDarker};
    width: 100%;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    box-sizing: border-box;

    & > .subforums {
      width: 100%;
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      grid-auto-rows: 1fr;
      grid-row-gap: 5px;

      & > div {
        width: 100%;
      }
    }

    & .buttons {
      padding-bottom: 10px;
    }

    & .subforum-filter-input {
      user-select: none;
      display: inline-flex;
      align-items: center;

      & label {
        display: inline-flex; /* aligns the text so in case it's multiline the next line doesn't start below the checkbox */
      }
    }
  }
`;

const placeholders = Array(10)
  .fill(1)
  .map((_, index) => (
    // eslint-disable-next-line react/no-array-index-key
    <Placeholder width={100} key={`p-${index}`} /> // Width gets overridden by style but it's required by the component
  ));

interface Subforum {
  id: number;
  name: string;
}

interface Props {
  subforumList: Subforum[];
  loadingSubforums: boolean;
  /**
   * [subforum_id] (if it exists in the array it should be filtered out)
   */
  tempFilteredSubforums: number[];
  setTempFilteredSubforums: (filteredSubforums: number[]) => void;
}

const SubforumFilters = ({
  subforumList,
  tempFilteredSubforums,
  setTempFilteredSubforums,
  loadingSubforums: initialLoading,
}: Props) => {
  const [checkAllState, setCheckAllState] = useState(CheckboxStates.Unchecked);

  // Keep the filter "select all" checkbox state synced when filters are changed
  useEffect(() => {
    const newValue = tempFilteredSubforums;
    if (newValue.length === 0) {
      setCheckAllState(CheckboxStates.Checked);
    } else if (newValue.length === subforumList.length) {
      setCheckAllState(CheckboxStates.Unchecked);
    } else {
      setCheckAllState(CheckboxStates.Indeterminate);
    }
  }, [tempFilteredSubforums, subforumList.length]);

  const onSubforumFilterChanged = (subforum, ev) => {
    let newValue;

    if (!ev.target.checked) {
      newValue = [...tempFilteredSubforums, subforum.id];
    } else {
      newValue = tempFilteredSubforums.filter((id) => id !== subforum.id);
    }

    setTempFilteredSubforums(newValue);
  };

  const onToggleSubforumFiltersChanged = () => {
    // Note: the value of the checkbox is the previous value.

    // eslint-disable-next-line default-case
    switch (checkAllState) {
      case CheckboxStates.Checked: {
        const subforumIds = subforumList.map((s) => s.id);
        setTempFilteredSubforums(subforumIds);
        setCheckAllState(CheckboxStates.Unchecked);
        break;
      }
      case CheckboxStates.Unchecked:
      case CheckboxStates.Indeterminate: {
        setTempFilteredSubforums([]);
        setCheckAllState(CheckboxStates.Checked);
        break;
      }
    }
  };

  return (
    <SubforumFilterWrapper>
      <div className="container">
        <div className="buttons">
          <Checkbox
            state={checkAllState}
            label="Select all"
            onChange={onToggleSubforumFiltersChanged}
          />
        </div>
        <div className="subforums">
          {initialLoading
            ? placeholders
            : subforumList.map((subforum) => (
                <div className="subforum-filter-input" key={subforum.id}>
                  <label>
                    <input
                      type="checkbox"
                      checked={tempFilteredSubforums.indexOf(subforum.id) === -1}
                      onChange={(e) => onSubforumFilterChanged(subforum, e)}
                    />
                    {subforum.name}
                  </label>
                </div>
              ))}
        </div>
      </div>
    </SubforumFilterWrapper>
  );
};

export default SubforumFilters;
