// test-utils.js
import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { ThemeProvider } from 'styled-components';
import { configureStore } from '../src/state/configureStore';

function customRender(
  ui,
  {
    initialState,
    path = '/',
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
    ...renderOptions
  } = {}
) {
  // eslint-disable-next-line react/prop-types
  function Wrapper({ children }) {
    return (
      <Router history={history}>
        <Provider store={configureStore(initialState)}>
          <ThemeProvider
            theme={{
              mode: 'dark',
              width: 'wide',
              scale: 'medium',
            }}
          >
            <Route path={path}>{children}</Route>
          </ThemeProvider>
        </Provider>
      </Router>
    );
  }
  return render(ui, { wrapper: Wrapper, ...renderOptions });
}

export * from '@testing-library/react';
export { customRender };
