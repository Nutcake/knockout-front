import dayjs from 'dayjs';
import React from 'react';
import { BASIC_USER, MODERATOR } from '../../../../src/utils/roleCodes';
import MessageMenu from '../../../../src/views/MessagesPage/components/MessageMenu';
import { customRender } from '../../../custom_renderer';

describe('MessageMenu component', () => {
  const mock = jest.fn();
  const conversations = [
    {
      id: 1,
      messages: [
        {
          id: 2,
          conversationId: 1,
          user: {
            id: 3,
            username: 'Joe',
            role: { code: BASIC_USER },
            avatarUrl: '',
          },
          content: 'stuff',
          readAt: dayjs().subtract(1, 'hour').toISOString(),
        },
      ],
      users: [
        {
          id: 4,
          username: 'Rick',
          role: { code: MODERATOR },
          avatarUrl: '',
        },
        {
          id: 3,
          username: 'Joe',
          role: { code: BASIC_USER },
          avatarUrl: '',
        },
      ],
      updatedAt: dayjs().subtract(3, 'hour').toISOString(),
    },
  ];

  it('displays conversation information', async () => {
    const { queryByText, queryByTitle } = customRender(
      <MessageMenu
        getData={mock}
        setModalOpen={mock}
        conversations={conversations}
        currentConversation={{ id: 1 }}
        setCurrentConversation={mock}
      />,
      { initialState: { user: { id: 4 } } }
    );
    expect(queryByText('Joe')).not.toBeNull();
    expect(queryByText('3h')).not.toBeNull();
    expect(queryByText('stuff')).not.toBeNull();
    expect(queryByTitle('Unread')).toBeNull();
  });

  it('displays an empty conversation list', async () => {
    const { queryByText } = customRender(
      <MessageMenu
        getData={mock}
        setModalOpen={mock}
        conversations={[]}
        currentConversation={{ id: 0 }}
        setCurrentConversation={mock}
      />,
      { initialState: { user: { id: 4 } } }
    );
    expect(queryByText('No messages')).not.toBeNull();
  });

  it('displays self messages', async () => {
    const { queryByText } = customRender(
      <MessageMenu
        getData={mock}
        setModalOpen={mock}
        conversations={conversations}
        currentConversation={{ id: 1 }}
        setCurrentConversation={mock}
      />,
      { initialState: { user: { id: 3 } } }
    );
    expect(queryByText('Rick')).not.toBeNull();
    expect(queryByText('You: stuff')).not.toBeNull();
  });

  it('displays new conversations', async () => {
    const { queryByText } = customRender(
      <MessageMenu
        getData={mock}
        setModalOpen={mock}
        conversations={[{ ...conversations[0], messages: [] }]}
        currentConversation={{ id: 1 }}
        setCurrentConversation={mock}
      />,
      { initialState: { user: { id: 3 } } }
    );
    expect(queryByText('New message')).not.toBeNull();
  });

  it('displays unread messages', async () => {
    const { queryByTitle } = customRender(
      <MessageMenu
        getData={mock}
        setModalOpen={mock}
        conversations={[
          { ...conversations[0], messages: [{ ...conversations[0].messages[0], readAt: null }] },
        ]}
        currentConversation={{ id: 1 }}
        setCurrentConversation={mock}
      />,
      { initialState: { user: { id: 4 } } }
    );
    expect(queryByTitle('Unread')).not.toBeNull();
  });
});
