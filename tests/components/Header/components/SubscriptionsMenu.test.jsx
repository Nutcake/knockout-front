/* eslint-disable no-underscore-dangle */
import React from 'react';
import { customRender, fireEvent } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import SubscriptionsMenu from '../../../../src/components/Header/components/SubscriptionsMenu';

describe('SubscriptionsMenu component', () => {
  const initialState = {
    subscriptions: {
      threads: {},
      count: 0,
    },
  };

  it('displays the menu when the icon is clicked', () => {
    const { getByTitle, queryByText } = customRender(<SubscriptionsMenu />, { initialState });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(queryByText('Subscriptions')).not.toBeNull();
  });

  it('displays an empty state', () => {
    const { getByTitle, queryByText } = customRender(<SubscriptionsMenu />, { initialState });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(queryByText('No new posts')).not.toBeNull();
  });

  it('displays threads with unread posts', async () => {
    localStorage.__STORE__.currentUser = '1';
    const state = {
      subscriptions: {
        threads: {
          '98': {
            title: 'Thread',
            page: 8,
            count: 3,
            postId: 109819,
            postNum: 147,
            iconId: 14,
            locked: false,
          },
          '313': {
            title: 'Second Thread',
            page: 25,
            count: 7,
            postId: 109815,
            postNum: 493,
            iconId: 2,
            locked: false,
          },
        },
        count: 4,
      },
    };
    const { getByTitle, queryByTitle, findByText } = customRender(<SubscriptionsMenu />, {
      initialState: state,
    });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(await findByText('Thread')).not.toBeNull();
    expect(await findByText('3')).not.toBeNull();
    expect(await findByText('Second Thread')).not.toBeNull();
    expect(await findByText('7')).not.toBeNull();
    expect(queryByTitle('Subscriptions').querySelector('.link-notification')).toHaveTextContent(
      '2'
    );
  });

  it('shows if a thread is locked', async () => {
    localStorage.__STORE__.currentUser = '1';
    const state = {
      subscriptions: {
        threads: {
          '98': {
            title: 'Ut et sed id vero magni molestiae.',
            page: 8,
            count: 1,
            postId: 109819,
            postNum: 147,
            iconId: 14,
            locked: false,
          },
          '313': {
            title: 'Aut sed consequuntur repellendus quis.',
            page: 25,
            count: 1,
            postId: 109815,
            postNum: 493,
            iconId: 2,
            locked: true,
          },
        },
        count: 4,
      },
    };
    const { getByTitle, findByTitle } = customRender(<SubscriptionsMenu />, {
      initialState: state,
    });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(await findByTitle('Locked')).not.toBeNull();
  });
});
